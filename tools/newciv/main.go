package main

import (
	"os"
	"fmt"
)

import "github.com/Splizard/pyrogenesis"

const (
	Civ = iota
	Name
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("Usage: newciv athens")
		return
	}
	
	var Civ = os.Args[1]
	
	_, err := pyrogenesis.LoadMod("public")
	if err != nil {
		fmt.Println("Could not load public mod!")
		return
	}

	realm, err := pyrogenesis.LoadMod("blockade")
	if err != nil {
		fmt.Println(err)
		return
	}
	
	var Structures = []string{
		"barracks", "civil_centre", "house", "corral", "sentry_tower",
		"defense_tower", "dock", "farmstead", "blacksmith",
		"field", "fortress", "stable", "storehouse", "market",
		"temple", "wall_gate", "wall_long", "wall_medium", "range",
		"wall_short", "wall_tower", "wonder", "workshop", "outpost",
	}
	
	for _, structure := range Structures {
		
		base, err := realm.LoadTemplate("structures/brit_"+structure)
		if err != nil {
			fmt.Println(err)
			return
		}
		
		merge, err := realm.LoadTemplate("structures/"+Civ+"_"+structure)
		if err != nil {
			fmt.Println(err)
			return
		}

		base.Fork("structures/"+Civ+"_"+structure)
		
		base.Set("Identity/Civ", Civ)
		base.Set("VisualActor/Actor", merge.Get("VisualActor/Actor"))
		base.Set("VisualActor/FoundationActor", merge.Get("VisualActor/FoundationActor"))
		base.SetNode("Footprint", merge.GetNode("Footprint"))
		base.SetNode("Obstruction", merge.GetNode("Obstruction"))
	
		base.Save()
	}
}
