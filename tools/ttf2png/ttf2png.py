#!/usr/bin/env python3
import sys
import os
import subprocess

from fontTools.ttLib import TTFont

TEXTS_DIR = "texts"
IMAGES_DIR = "images"

TTF_PATH = sys.argv[1]
FONT_SIZE = sys.argv[2]
TTF_NAME, TTF_EXT = os.path.splitext(os.path.basename(TTF_PATH))

ttf = TTFont(TTF_PATH, 0, verbose=0, allowVID=0, ignoreDecompileErrors=True, fontNumber=-1)

for d in [TEXTS_DIR, IMAGES_DIR]:
    if not os.path.isdir(d):
        os.mkdir(d)

for letter in ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", " " , "`", "-"]:
        char_unicode = letter
        char_utf8 = char_unicode.encode('utf_8')
        char_name = letter
        f = open(os.path.join(TEXTS_DIR, char_name + '.txt'), 'w')
        f.write(char_utf8.decode("utf-8"))
        f.close()
ttf.close()

files = os.listdir(TEXTS_DIR)
for filename in files:
    name, ext = os.path.splitext(filename)
    input_txt = TEXTS_DIR + "/" + filename
    
    if not name.lower() == name.upper():
        
        if name.lower() == name:
            name = name.lower()
        else:
            name = name.lower()+name.lower()

    output_png = IMAGES_DIR + "/" + name + ".png"    
    subprocess.call(["convert", "-font", TTF_PATH, "-fill", "white", "-pointsize", FONT_SIZE, "-background", "rgba(0,0,0,0)", "label:@" + input_txt, output_png])

print("finished")
