package main

import "github.com/anacrolix/torrent"
import "github.com/pkg/browser"
import "github.com/gorilla/websocket"
import "gopkg.in/src-d/go-git.v4"
import "github.com/Splizard/pyrogenesis"

import (
	"net/http"
	"log"
	"os/exec"
	"time"
	"fmt"
	"os"
	"io/ioutil"
	"encoding/json"
)

var upgrader = websocket.Upgrader{
	
	CheckOrigin: func(r *http.Request) bool {
		return r.Header.Get("Origin") == "https://realmoforder.com"
	},
	
} // use default options


const InstallerName = `0ad-0.0.23-alpha-win32.exe`


var state = "uninstalled"

func DetectState() {
	if _, err := os.Stat(InstallerName); !os.IsNotExist(err) {
		state = "downloaded"
	}
}

var downloading = false
func download(connection *websocket.Conn) {
	if (downloading) {
		return
	}
	downloading = true
	client, _ := torrent.NewClient(nil)
	defer client.Close()
	torrent, _ := client.AddMagnet("magnet:?xt=urn:btih:e66a4037fb1d423aca7cdfc802ea46d8846105d8&dn=0ad-0.0.23-alpha-win32.exe&tr=http%3a%2f%2freleases.wildfiregames.com%3a2710%2fannounce&ws=http%3a%2f%2freleases.wildfiregames.com%2f0ad-0.0.23-alpha-win32.exe%3ftorrent")
	<-torrent.GotInfo()
	
	ticker := time.NewTicker(1000 * time.Millisecond)
	go func() {
		for range ticker.C {
			var status = fmt.Sprint("progress=", 100*(1-float64(torrent.BytesMissing())/float64(torrent.Length())))
			connection.WriteMessage(websocket.TextMessage, []byte(status))
			
			if torrent.BytesMissing() == 0 {
				state = "downloaded"
				return
			}
		}
	}()

	torrent.DownloadAll()
}

var installing = false
func install(connection *websocket.Conn) {
	if (installing) {
		return
	}
	installing = true
	
	var cloned = make(chan bool)
	go func() {
		_, err := git.PlainClone(pyrogenesis.ModsPath+"/blockade", false, &git.CloneOptions{
			URL:      "https://bitbucket.org/pandamauts/blockade",
			Progress: os.Stdout,
		})
		if err != nil {
			fmt.Println(err)
		}
		
		cloned <- true
	}()

	exec.Command(InstallerName, "/S").Run()
	<- cloned
	
	state = "installed"
	connection.WriteMessage(websocket.TextMessage, []byte("installed"))
}

func server(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	
	DetectState()
	
	c.WriteMessage(websocket.TextMessage, []byte(state))
	
	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		
		if string(message) == "play" {
			go roo()
		}
		
		if string(message) == "download" {
			download(c)
		}
		
		if string(message) == "install" {
			install(c)
		}
	}
}

type Arguments struct {
	LoadLastSavedGame bool `json:,omitempty`
}

func roo() {
	var args Arguments
	var Mod = "blockade"
	
	var MapName = "BCT the canyon"
	if len(os.Args) == 1 {
		
		fmt.Println("Random map")
		err := pyrogenesis.Run("-mod=public", "-autostart-seed=-1", "-mod="+Mod, "-autostart-players=1", "-autostart=random/aegean_sea", "-autostart-civ=1:brit")
		if err != nil {
			fmt.Println(err)
		}
		return
		
	}
	
	if len(os.Args) > 1 {
		MapName = os.Args[1]
	}
	
	if MapName == "load" {
		
		args.LoadLastSavedGame = true
		
		file, err := os.Create(pyrogenesis.ModsPath+"/"+Mod+"/Arguments.json")
		if err == nil {
			err = json.NewEncoder(file).Encode(args)
			if err == nil {			
				err = pyrogenesis.Run("-mod=public", "-mod="+Mod)
				if err != nil {
					fmt.Println(err)
				}
				
				file.Close()
				ioutil.WriteFile(pyrogenesis.ModsPath+"/"+Mod+"/Arguments.json", []byte("{}"), 0666)
				
				return
			}
		}
		
	} else {
		
		if _, err := os.Stat(pyrogenesis.ModsPath+"/"+Mod+"/maps/scenarios/"+MapName+".xml"); os.IsNotExist(err) {
			fmt.Println("Failed to load '"+MapName+"', map does not exist!")
			os.Exit(1)
		}
		
	}
	
	err := pyrogenesis.Run("-mod=public", "-autostart-seed=-1", "-mod="+Mod, "-autostart=scenarios/"+MapName)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	if len(os.Args) <= 1 {
		browser.OpenURL("https://realmoforder.com/play/")
		http.HandleFunc("/", server)
		log.Fatal(http.ListenAndServe(":3131", nil))
	} else {
		roo()
	}
}
