package main

import "github.com/beevik/etree"

func Delete(element *etree.Element, query string) {
	var unlucky_element = element.FindElement(query)
	if unlucky_element != nil {
		unlucky_element.Parent().RemoveChild(unlucky_element)
	}
}
