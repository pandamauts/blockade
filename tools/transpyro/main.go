package main

import "github.com/Splizard/pyrogenesis"

import (
	"fmt"
	"strings"
)

func main() {
	mod_name := "blockade"

	new_actor := "units/brit_support_test"
	base_unit_template := "units/brit_support_female_citizen"
	template_with_target_actor := "units/brit_support_female_citizen"
	output_template := "units/brit_support_test"

	_, err := pyrogenesis.LoadMod("public")
	if err != nil {
		fmt.Println("Could not load public mod!")
		return
	}

	mod, err := pyrogenesis.LoadMod(mod_name)
	if err != nil {
		fmt.Println(err)
		return
	}

	base, err := mod.LoadTemplate(base_unit_template)
	if err != nil {
		fmt.Println(err)
		return
	}

	target, err := mod.LoadTemplate(template_with_target_actor)
	if err != nil {
		fmt.Println(err)
		return
	}

	target_actor := target.Get("VisualActor/Actor")
	if target_actor == "" {
		fmt.Println("Target template does not have a visual actor!")
		return
	}

	//We want to copy this element with a few modifications.
	actor, err := mod.LoadActor(target_actor)
	if err != nil {
		fmt.Println(err)
		return
	}

	//Print the actor so you can figure out how to change it.

	xml, err := actor.Tree().WriteToString()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(xml)

	first := actor.Tree().FindElement("//group/variant")

//Change prop points here:
  Delete(first, "//prop[@attachpoint='weapon_R']")
	Delete(first, "//prop[@attachpoint='weapon_L']")
  Delete(first, "//prop[@attachpoint='ammo']")
	Delete(first, "//prop[@attachpoint='back']")

//Change animations here:
	elements := actor.Tree().FindElements("//variant[@file]")
	for _, element := range elements {
		if element == first {
			continue
		}

		file := element.SelectAttr("file").Value

		if strings.Contains(file, "base") {
			element.SelectAttr("file").Value = "biped/base_slave.xml"
		}

		if strings.Contains(file, "attack_ranged") {
			element.Parent().RemoveChild(element)
		}
	}

	actor.Fork(new_actor)
	actor.Save()

	base.Fork(output_template)
	base.Set("VisualActor/Actor", new_actor+".xml")

	base.Save()
}
