package main

import (
	"os"
	"fmt"
	"encoding/csv"
	"strings"
)

import "github.com/Splizard/pyrogenesis"

const (
	Civ = iota
	Name
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("Usage: sinksheet input.csv")
		return
	}
	
	file, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}
	
	reader := csv.NewReader(file)
	
	realm, err := pyrogenesis.LoadMod("blockade")
	if err != nil {
		fmt.Println(err)
		return
	}
	
	Headings, err := reader.Read()
	if err != nil {
		fmt.Println(err)
		return
	}
	
	for i := range Headings {
		if i < 2 || !strings.Contains(Headings[i], "=") {
			Headings[i] = strings.TrimSpace(Headings[i])
			continue
		}
		Headings[i] = strings.TrimSpace(strings.Split(Headings[i], "=")[1])
	}
	
	for {
		
		data, err := reader.Read()
		if err != nil {
			break
		}
	
		template_name := "structures/"+data[Civ]+"_"+strings.ToLower(data[Name])
		
		template_name = strings.Replace(template_name, " ", "_", -1) 

		building, err := realm.LoadTemplate(template_name)
		if err != nil {
			fmt.Println(err)
			return
		}
		
		var changed = false
		
		for i := range Headings {
			if i < 2 || Headings[i] == "" || strings.TrimSpace(data[i]) == "" {
				continue
			}
			
			if (building.Get(Headings[i]) != data[i]) {
				building.Set(Headings[i], data[i])
				changed = true
			}
		}
		
		if changed {
			fmt.Println(template_name, " updated ✓")
			building.Save()
		}
	}
}
