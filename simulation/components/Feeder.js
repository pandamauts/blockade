let Feeder = {}
 
Feeder.Schema = "<a:help>Deals with feeding units within range</a:help>" +
	"<a:example>" +
		"<Feeds>1</Feeds>" +
	"</a:example>" +
	
	"<element name='Feeds' a:help='The number of units this unit can feed within its radius'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>";
	
Feeder.Init = function() {
	this.feeding = [];
}

Feeder.Feed = function (ent) {
	if (this.feeding.length < this.template.Feeds) {
		this.feeding.push(ent);
		return true;
	}
	return false;
}

Feeder.StopFeeding = function(ent) {
	for (let i = 0; i < this.feeding.length; i++) {
		if (this.feeding[i] == ent) {
			this.feeding[i] = this.feeding[this.feeding.length-1];
			this.feeding.pop();
			return;
		}
	}
}

rts.RegisterComponent(IID_Feeder, "Feeder", Feeder);
