Health.prototype.CreateDeathSpawnedEntity = function()
{
	// If the unit died while not in the world, don't spawn a death entity for it
	// since there's nowhere for it to be placed
	var cmpPosition = Engine.QueryInterface(this.entity, IID_Position);
	if (!cmpPosition.IsInWorld())
		return INVALID_ENTITY;

	// Create SpawnEntityOnDeath entity
	var spawnedEntity = Engine.AddLocalEntity(this.template.SpawnEntityOnDeath);

	// Move to same position
	var cmpSpawnedPosition = Engine.QueryInterface(spawnedEntity, IID_Position);
	var pos = cmpPosition.GetPosition();
	cmpSpawnedPosition.JumpTo(pos.x, pos.z);
	var rot = cmpPosition.GetRotation();
	cmpSpawnedPosition.SetYRotation(rot.y);
	cmpSpawnedPosition.SetXZRotation(rot.x, rot.z);

	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	var cmpSpawnedOwnership = Engine.QueryInterface(spawnedEntity, IID_Ownership);
	if (cmpOwnership && cmpSpawnedOwnership)
		cmpSpawnedOwnership.SetOwner(cmpOwnership.GetOwner());
	
	
	//Copy over stone resource requirements!? HACK
	let Cost = Engine.QueryInterface(this.entity, IID_Cost);
	if (Cost) {
		
		let Pot = rts.Get(IID_ResourcePot);
		Pot.CountingStone = false;
		
		let costs = Cost.GetResourceCosts();
		if ("stone" in costs && costs["stone"] > 0) {
			
			let ResourceSupply = Engine.QueryInterface(spawnedEntity, IID_ResourceSupply);
			if (ResourceSupply) {
				ResourceSupply.amount = costs["stone"];
				ResourceSupply.max = costs["stone"];
			}
			
		}
	}
	

	return spawnedEntity;
}; 

Health.prototype.OnValueModification = function(msg)
{
};
