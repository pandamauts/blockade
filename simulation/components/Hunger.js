let Hunger = {}

Hunger.Schema = 

	"<a:help>Deals with hunger and food consumption.</a:help>" +
	"<a:example>" +
		"<StarveTime>2</StarveTime>" +
		"<TickTime>6</TickTime>" +
		"<Appetite>1</Appetite>" +
		"<FoodRange>40</FoodRange>" +
	"</a:example>" +

	"<element name='StarveTime' a:help='The amount of meals this unit can skip until it is starving'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>" +
	
	"<element name='TickTime' a:help='The tick time (in seconds) for how often this unit checks for food when starving'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>" +
	
	"<element name='FoodRange' a:help='The maximum distance a food supply building can be in order for this unit to be able to consume food.'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>" +
	
	"<element name='Appetite' a:help='How many pieces of food this unit must eat every consumption'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>";

Hunger.Init = function() {
	this.hungry = false;
	this.timer = null;
	
	this.SkippedMeals = 0;
	
	this.starving = false;
};

Hunger.OnOwnershipChanged = function() {	
	if ( this.GetOwner() != rts.Gaia && !this.timer ) {

		this.timer = this.NewTimer().Run("Eat").Every(+this.template.TickTime * 1000).Start();
		
	}	
}

//Need to check for food supply buildings nearby! Eat the supplies.
Hunger.Eat = function() {
	let dropsites;
	
	let Garrisonable = this.Get(IID_Garrisonable);
	if (Garrisonable) {
		let building = Garrisonable.GetBuilding();
		if (building) {
			let dropsite = rts.Call(IID_ResourceDropsite, building);
			if (dropsite) {
				dropsites = [dropsite];
			}
		}
	}
	
	if (!dropsites) {
		dropsites = rts.Find(IID_ResourceDropsite).Near(this).Radius(+this.template.FoodRange).List();
	}
	
	let Dropsite = this.Get(IID_ResourceDropsite);
	if (Dropsite) {
		dropsites.unshift(Dropsite);
	}

	for ( let i = 0; i < dropsites.length; i++ ) {

		let dropsite = dropsites[i];
		
		if ( dropsite.HasFood() ) {

			let TimeUntilNextMeal = dropsite.EatFood()/(+this.template.Appetite);
			Engine.PostMessage(this.entity, MT_Full, null);
			
			rts.Get(IID_Timer).UpdateTimer(this.timer, TimeUntilNextMeal);

			this.SkippedMeals = 0;
			this.hungry = false;
			this.starving = false;
			
			this.RefreshStatusBars();
			return;

		}
	}
	
	//Check if we have food ourselves!
	let ResourceGatherer = this.Get(IID_ResourceGatherer);

	if ( ResourceGatherer && ResourceGatherer.HasFood() ) {
		
		let TimeUntilNextMeal = ResourceGatherer.EatFood()/(+this.template.Appetite);
		Engine.PostMessage(this.entity, MT_Full, null);
		
		rts.Get(IID_Timer).UpdateTimer(this.timer, TimeUntilNextMeal);
		
		this.SkippedMeals = 0;
		this.hungry = false;
		this.starving = false;
		
		this.RefreshStatusBars();
		return;
	}
	
	this.SkippedMeals++;
	
	if (!this.hungry) {
		Engine.PostMessage(this.entity, MT_Hungry, null);
		rts.Get(IID_Timer).UpdateTimer(this.timer, +this.template.TickTime * 1000)
		
		this.hungry = true;
		
		this.RefreshStatusBars();
	}
	
	if (!this.starving) {
		if (this.SkippedMeals >= +this.template.StarveTime) {
			Engine.PostMessage(this.entity, MT_Starving, null);
			this.starving = true;
		}
	}
	
	if (this.hungry) {
		rts.Get(IID_HintsManager).HungerHint(this.GetOwner());
	}
}

Hunger.Information = function() {
	return {
		Hungry: this.hungry,
		Level: this.SkippedMeals,
		Max: (+this.template.StarveTime) / (+this.template.TickTime),
	};
}

Hunger.DetectActor = function() {
	/*let list = rts.Find(0).Near(this).OwnedBy(-1).Radius(10).List();
	
	print(list.length);
	
	for (let ent of list) {
		print(ent);
		print("name", Engine.QueryInterface(SYSTEM_ENTITY, IID_TemplateManager).GetCurrentTemplateName(ent), "\n");
	}*/
	
	
	Engine.PostMessage(this.entity, MT_Starving, null);
}

//User Interface.
Hunger.Commands = {};
Hunger.Commands.Feed = "Eat";
Hunger.Commands.DetectActor = "DetectActor";

rts.RegisterComponent(IID_Hunger, "Hunger", Hunger);
