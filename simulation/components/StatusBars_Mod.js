StatusBars.prototype.RegenerateSprites = function()
{
	let cmpOverlayRenderer = Engine.QueryInterface(this.entity, IID_OverlayRenderer);
	cmpOverlayRenderer.Reset();

	let yoffset = 0;
	for (let sprite of this.Sprites)
		yoffset += this["Add" + sprite](cmpOverlayRenderer, yoffset);
	
	if (this.enabled) {
		for (let overlay of rts.Overlays) {
			overlay(this.entity, cmpOverlayRenderer);
		}
	}
	
	for (let overlay of rts.EvilOverlays) {
		overlay(this.entity, cmpOverlayRenderer);
	}
	
	//Range overlay.
	let RangeOverlayRenderer = Engine.QueryInterface(this.entity, IID_RangeOverlayRenderer);
	if (!RangeOverlayRenderer)
		return;

	RangeOverlayRenderer.ResetRangeOverlays();
	
	if (this.enabled) {
		
		
		let ResourceDropsite = rts.Call(IID_ResourceDropsite, this.entity);
		
		if (ResourceDropsite && ResourceDropsite.HasFood()) {
			RangeOverlayRenderer.AddRangeOverlay(
					80,
					"outline_border.png",
					"outline_border_mask.png",
					0.2);
		}
	}
};

//API method, register statusbar.
StatusBars.prototype.Register = function(name, definition) {
	
	if (StatusBars.prototype["Add"+name+"Bar"]) {
		return;
	}
	
	StatusBars.prototype.Sprites.unshift(name+"Bar")
	StatusBars.prototype["Add"+name+"Bar"] = function(cmpOverlayRenderer, yoffset) {
		if (!this.enabled)
			return 0;

		return this.AddBar(cmpOverlayRenderer, yoffset, name, definition.GetValue(this.entity) / definition.GetMax(this.entity));
	}
}; 

StatusBars.prototype.Sprites = [
	"PackBar",
	"AuraIcons",
	"RankIcon"
];
