let Builds = {};

Builds.Schema = 
	"<a:help>When selected this unit can create these structures</a:help>" +
	"<a:example>" +
		"<Housing datatype='tokens'>" +
			"structures/{native}_house" +
		"</Housing>" +
	"</a:example>" +
	"<element name='Housing' a:help='Housing category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Food' a:help='Food category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Trees' a:help='Trees category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Economy' a:help='Economy category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Military' a:help='Military category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Defense' a:help='Defence category'>" +
			"<text/>" +
	"</element>" +
	"<element name='Props' a:help='Props category'>" +
			"<text/>" +
	"</element>";

Builds.Init = function() {
	this.BuildList = null;
}

Builds.SelectBuildList = function(cmd) {
	let Player = this;
	
	if (cmd.Index == -1) {
		Player.BuildList = null;
		return;
	}
	
	if (cmd.Index == 0 && this.template.Housing) {
		Player.BuildList = this.template.Housing.split(/\s+/);
	}
	if (cmd.Index == 1 && this.template.Food) {
		Player.BuildList = this.template.Food.split(/\s+/);
	}
	if (cmd.Index == 2 && this.template.Trees) {
		Player.BuildList = this.template.Trees.split(/\s+/);
	}
	if (cmd.Index == 3 && this.template.Economy) {
		Player.BuildList = this.template.Economy.split(/\s+/);
	}
	if (cmd.Index == 4 && this.template.Military) {
		Player.BuildList = this.template.Military.split(/\s+/);
	}
	if (cmd.Index == 5 && this.template.Defense) {
		Player.BuildList = this.template.Defense.split(/\s+/);
	}
	if (cmd.Index == 6 && this.template.Props) {
		Player.BuildList = this.template.Props.split(/\s+/);
	}
	
	if (!Player.BuildList) Player.BuildList = [];
}

Builds.Information = function() {
	let Player = this;
	
	return {
		List: Player.BuildList,
	};
}

Builds.Commands = {};
Builds.Commands.SelectBuildList = "SelectBuildList";

rts.RegisterComponent(IID_Builds, "Builds", Builds)
