function Garrisonable() {}

Garrisonable.prototype.Schema = "<empty/>";

Garrisonable.prototype.Init = function()
{
	this.building = null;
};

Garrisonable.prototype.GetBuilding = function() {
	return this.building;
}

Garrisonable.prototype.SetBuilding = function(ent) {
	this.building = ent;
}

Engine.RegisterComponentType(IID_Garrisonable, "Garrisonable", Garrisonable);
