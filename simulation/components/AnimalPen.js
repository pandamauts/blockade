let AnimalPen = {};

AnimalPen.Schema = "<a:help>Deals with spawning animals</a:help>" +
	"<a:example>" +
		"<Slots>1</Slots>" +
	"</a:example>" +
	
	"<element name='Slots' a:help='The number of *finite but renewable* animals that can spawn from this pen.'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>";

AnimalPen.Init = function() {
	this.male = null;
	this.female = null;
	
	this.slots = null;
	this.icon = "";
}

AnimalPen.FillSlot = function() {
	if (this.male) {
		this.icon = rts.Call(IID_Identity, this.male).template.Icon;
		return this.Spawn(rts.GetTemplateName(this.male));
	}
}

AnimalPen.Fill = function() {
	if (this.slots == null) {
		this.slots = [];
		for (let i = 0; i < +this.template.Slots; i++) {
			this.slots.push(this.FillSlot());
		}
		return
	}
}

AnimalPen.OnGarrisonedUnitsChanged = function (msg) {
	if (msg.added && msg.added.length > 0) {
		for (let entity of msg.added) {
			
			let Identity = rts.Call(IID_Identity, entity);
			let gender = Identity.GetGender();
			
			if (gender == "male" && !this.male) {
				
				this.male = entity;
				
				this.Attach(entity, {x:2, y:0, z:2})
					
			} else if (gender == "female" && !this.female) {
				
				this.female = entity;
				
				this.Attach(entity, {x:-2, y:0, z:2})
			} else {
				this.Attach(entity, {x:0, y:0, z:2})
			}
			
			
		}
	}
	if (msg.removed && msg.removed.length > 0) {
		for (let entity of msg.removed) {
			if (entity == this.male) this.male = null;
			if (entity == this.female) this.female = null;
			rts.Detach(entity, {x:2, y:2, z:2})
		}
	}
}

AnimalPen.Commands = {};
AnimalPen.Commands.Fill = "Fill";

AnimalPen.Information = function() {
	return {
		Slots: this.slots,
		ReadyToBreed: this.male && this.female,
		Icon: this.icon
	};
}

rts.RegisterComponent(IID_AnimalPen, "AnimalPen", AnimalPen)
