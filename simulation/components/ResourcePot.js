let ResourcePot = {};

ResourcePot.Schema = rts.Schema.System;

ResourcePot.Init = function() {
	this.resources = {};
	this.CountingStone = true;
}

ResourcePot.Add = function(resource, amount) {
	if (!(resource in this.resources)) this.resources[resource] = 0;
	
	this.resources[resource] += amount;
}

ResourcePot.Take = function(resource, amount) {
	this.resources[resource] -= amount;
}

//Does not support negative resources!
ResourcePot.Has = function(resource, amount) {
	if (!(resource in this.resources)) return false;
	
	return this.resources[resource] >= amount;
}

rts.RegisterSystemComponent(IID_ResourcePot, "ResourcePot", ResourcePot);
