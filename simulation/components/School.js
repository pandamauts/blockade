let School = {};

School.template = {
	Component: 	"text",
	Property: "text",
	Operation: "text",
	Value: "text",
	Condition: "text"
}

School.OnCreate = function() {
	this.component = eval(this.template.Component);
}

School.Teach = function(entity) {
	let Component = rts.Call(this.component, entity);
			
	if (Component) {
		
		let condition = false;
		switch (this.template.Condition) {
			case "smaller":
				if (Component[this.template.Property] < this.template.Value) {
					condition = true
				}
				break;
		}
		
		if (condition) return;
		
		switch (this.template.Operation) {
			case "equals":
				Component[this.template.Property] = this.template.Value;
				break;
		}
	}
	
	PlaySound("levelup", this.entity);
	this.Get(IID_GarrisonHolder).PerformEject([entity], false);
}

School.OnGarrisonedUnitsChanged = function (msg) {
	if (msg.added.length > 0) {
		for (let entity of msg.added) {
			
			this.NewTimer().Run("Teach", entity).After(1000).Start();
			
		}
	}
}

rts.RegisterComponent(IID_School, "School", School);
