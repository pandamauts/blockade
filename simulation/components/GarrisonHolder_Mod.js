
/**
 * @return {boolean} Whether the entity was garrisonned.
 */
GarrisonHolder.prototype.PerformGarrison = function(entity)
{
	if (!this.HasEnoughHealth())
		return false;

	// Check if the unit is allowed to be garrisoned inside the building
	if (!this.IsAllowedToGarrison(entity))
		return false;

	// Check the capacity
	let extraCount = 0;
	let cmpGarrisonHolder = Engine.QueryInterface(entity, IID_GarrisonHolder);
	if (cmpGarrisonHolder)
		extraCount += cmpGarrisonHolder.GetGarrisonedEntitiesCount();
	if (this.GetGarrisonedEntitiesCount() + extraCount >= this.GetCapacity())
		return false;

	if (!this.timer && this.GetHealRate() > 0)
	{
		let cmpTimer = Engine.QueryInterface(SYSTEM_ENTITY, IID_Timer);
		this.timer = cmpTimer.SetTimeout(this.entity, IID_GarrisonHolder, "HealTimeout", 1000, {});
	}

	// Actual garrisoning happens here
	this.entities.push(entity);
	this.UpdateGarrisonFlag();
	let cmpProductionQueue = Engine.QueryInterface(entity, IID_ProductionQueue);
	if (cmpProductionQueue)
		cmpProductionQueue.PauseProduction();

	let cmpAura = Engine.QueryInterface(entity, IID_Auras);
	if (cmpAura && cmpAura.HasGarrisonAura())
		cmpAura.ApplyGarrisonBonus(this.entity);
	
	Engine.QueryInterface(entity, IID_Garrisonable).SetBuilding(this.entity);
	return true;
};

/**
 * Garrison a unit inside. The timer for AutoHeal is started here.
 * @param {number} vgpEntity - The visual garrison point that will be used.
 * If vgpEntity is given, this visualGarrisonPoint will be used for the entity.
 * @return {boolean} Whether the entity was garrisonned.
 */
GarrisonHolder.prototype.Garrison = function(entity, vgpEntity)
{
	let cmpPosition = Engine.QueryInterface(entity, IID_Position);
	if (!cmpPosition)
		return false;

	if (!this.PerformGarrison(entity))
		return false;

	let visibleGarrisonPoint = vgpEntity;
	if (!visibleGarrisonPoint)
		for (let vgp of this.visibleGarrisonPoints)
		{
			if (vgp.entity)
				continue;
			visibleGarrisonPoint = vgp;
			break;
		}

	if (visibleGarrisonPoint)
	{
		visibleGarrisonPoint.entity = entity;
		// Angle of turrets:
		// Renamed entities (vgpEntity != undefined) should keep their angle.
		// Otherwise if an angle is given in the visibleGarrisonPoint, use it.
		// If no such angle given (usually walls for which outside/inside not well defined), we keep
		// the current angle as it was used for garrisoning and thus quite often was from inside to
		// outside, except when garrisoning from outWorld where we take as default PI.
		let cmpTurretPosition = Engine.QueryInterface(this.entity, IID_Position);
		if (!vgpEntity && visibleGarrisonPoint.angle != null)
			cmpPosition.SetYRotation(cmpTurretPosition.GetRotation().y + visibleGarrisonPoint.angle);
		else if (!vgpEntity && !cmpPosition.IsInWorld())
			cmpPosition.SetYRotation(cmpTurretPosition.GetRotation().y + Math.PI);
		let cmpUnitMotion = Engine.QueryInterface(entity, IID_UnitMotion);
		if (cmpUnitMotion)
			cmpUnitMotion.SetFacePointAfterMove(false);
		cmpPosition.SetTurretParent(this.entity, visibleGarrisonPoint.offset);
		let cmpUnitAI = Engine.QueryInterface(entity, IID_UnitAI);
		if (cmpUnitAI)
			cmpUnitAI.SetTurretStance();
	}
	else
		cmpPosition.MoveOutOfWorld();

	Engine.PostMessage(this.entity, MT_GarrisonedUnitsChanged, { "added": [entity], "removed": [] });
	
	return true;
};

/**
 * Simply eject the unit from the garrisoning entity without moving it
 * @param {number} entity - Id of the entity to be ejected.
 * @param {boolean} forced - Whether eject is forced (i.e. if building is destroyed).
 * @return {boolean} Whether the entity was ejected.
 */
GarrisonHolder.prototype.Eject = function(entity, forced)
{
	let entityIndex = this.entities.indexOf(entity);
	// Error: invalid entity ID, usually it's already been ejected
	if (entityIndex == -1)
		return false;

	// Find spawning location
	let cmpFootprint = Engine.QueryInterface(this.entity, IID_Footprint);
	let cmpHealth = Engine.QueryInterface(this.entity, IID_Health);
	let cmpIdentity = Engine.QueryInterface(this.entity, IID_Identity);

	// If the garrisonHolder is a sinking ship, restrict the location to the intersection of both passabilities
	// TODO: should use passability classes to be more generic
	let pos;
	if ((!cmpHealth || cmpHealth.GetHitpoints() == 0) && cmpIdentity && cmpIdentity.HasClass("Ship"))
		pos = cmpFootprint.PickSpawnPointBothPass(entity);
	else
		pos = cmpFootprint.PickSpawnPoint(entity);

	if (pos.y < 0)
	{
		// Error: couldn't find space satisfying the unit's passability criteria
		if (!forced)
			return false;

		// If ejection is forced, we need to continue, so use center of the building
		let cmpPosition = Engine.QueryInterface(this.entity, IID_Position);
		pos = cmpPosition.GetPosition();
	}

	this.entities.splice(entityIndex, 1);
	let cmpEntPosition = Engine.QueryInterface(entity, IID_Position);
	let cmpEntUnitAI = Engine.QueryInterface(entity, IID_UnitAI);

	for (let vgp of this.visibleGarrisonPoints)
	{
		if (vgp.entity != entity)
			continue;
		cmpEntPosition.SetTurretParent(INVALID_ENTITY, new Vector3D());
		let cmpEntUnitMotion = Engine.QueryInterface(entity, IID_UnitMotion);
		if (cmpEntUnitMotion)
			cmpEntUnitMotion.SetFacePointAfterMove(true);
		if (cmpEntUnitAI)
			cmpEntUnitAI.ResetTurretStance();
		vgp.entity = null;
		break;
	}

	if (cmpEntUnitAI)
		cmpEntUnitAI.Ungarrison();

	let cmpEntProductionQueue = Engine.QueryInterface(entity, IID_ProductionQueue);
	if (cmpEntProductionQueue)
		cmpEntProductionQueue.UnpauseProduction();

	let cmpEntAura = Engine.QueryInterface(entity, IID_Auras);
	if (cmpEntAura && cmpEntAura.HasGarrisonAura())
		cmpEntAura.RemoveGarrisonBonus(this.entity);

	cmpEntPosition.JumpTo(pos.x, pos.z);
	cmpEntPosition.SetHeightOffset(0);

	let cmpPosition = Engine.QueryInterface(this.entity, IID_Position);
	if (cmpPosition)
		cmpEntPosition.SetYRotation(cmpPosition.GetPosition().horizAngleTo(pos));

	Engine.PostMessage(this.entity, MT_GarrisonedUnitsChanged, { "added": [], "removed": [entity] });
	
	Engine.QueryInterface(entity, IID_Garrisonable).SetBuilding(null);

	return true;
};

