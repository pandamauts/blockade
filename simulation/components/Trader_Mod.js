
g_Commands["ChangeTrade"] = function(player, cmd, data) {
	for (let ent of data.entities) {
		
		let Trader = Engine.QueryInterface(ent, IID_Trader);
		if (!Trader) return;
		
		let Identity = Engine.QueryInterface(ent, IID_Identity);
		if (Identity)
			Identity.name = cmd.resource+" Trader";
		
		//Change trader specialisation.
		Trader.specialisation = cmd.resource;
	};
};

Trader.prototype.AddResources = function(ent, gain)
{
	let ResourceDropsite = Engine.QueryInterface(ent, IID_ResourceDropsite);
	if (ResourceDropsite)
		ResourceDropsite.GiveResource({type: this.goods.type, amount: gain});

	//let cmpStatisticsTracker = QueryOwnerInterface(ent, IID_StatisticsTracker);
	//if (cmpStatisticsTracker)
		//cmpStatisticsTracker.IncreaseTradeIncomeCounter(gain);
};

Trader.prototype.GenerateResources = function(currentMarket, nextMarket)
{
	this.AddResources(currentMarket, this.goods.amount.traderGain);
};

Trader.prototype.PerformTrade = function(currentMarket)
{
	let previousMarket = this.markets[this.index];
	if (previousMarket != currentMarket)  // Inconsistent markets
	{
		this.goods.amount = null;
		return INVALID_ENTITY;
	}

	this.index = ++this.index % this.markets.length;
	let nextMarket = this.markets[this.index];

	let cmpPlayer = QueryOwnerInterface(this.entity);
	if (!cmpPlayer)
		return INVALID_ENTITY;

	this.goods.type = cmpPlayer.GetNextTradingGoods();
	this.goods.amount = this.CalculateGain(currentMarket, nextMarket);
	this.goods.origin = currentMarket;
	
	
	//Take/Give resources.
	let Goods = Engine.QueryInterface(this.entity, IID_ResourceDropsite);
	let Dropsite = Engine.QueryInterface(currentMarket, IID_ResourceDropsite);
	
	if (!Goods || !Dropsite) {
		warn("Goods or Dropsite for trader is nil!");
		return nextMarket;
	}
	
	let specialisation = this.specialisation;
	if (!this.specialisation) specialisation = "food";
	
	let Capacity = +Goods.template.Max;
	
	//Taking.
	if (this.index == 1) {
		if (Dropsite.Has(specialisation, Capacity)) {
			Goods.GiveResource({type: specialisation, amount: Capacity});
			Dropsite.Take(specialisation, Capacity);
			
		} else if (Dropsite.Has(specialisation, 1)) {
			Goods.GiveResource({type: specialisation, amount: Dropsite.stored[specialisation]});
			Dropsite.Take(specialisation, Dropsite.stored[specialisation]);
		}
	}
		
	//Giving.
	if (this.index == 0 && Dropsite.AcceptsType(specialisation)) {		
		if (Goods.Has(specialisation, Capacity)) {
			Dropsite.GiveResource({type: specialisation, amount: Capacity});
			Goods.Take(specialisation, Capacity);
			
		} else if (Goods.Has(specialisation, 1)) {
			Dropsite.GiveResource({type: specialisation, amount: Goods.stored[specialisation]});
			Goods.stored[specialisation] = 0;
		}
	}
	
	let inventory = Goods.stored["food"] || 0;
	if (inventory <= Capacity/2) {
		if (Dropsite.HasFood(Capacity/2-inventory)) {
			Goods.GiveResource({type: "food", amount: Capacity/2-inventory});
			Dropsite.EatFood(Capacity/2-inventory);
		}
	}
	
	return nextMarket;
};

