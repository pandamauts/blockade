let Training = {};

Training.Schema = 
	"<optional>" +
		"<element name='Job' a:help='The job that units who train here get'>" +
			"<text/>" +
		"</element>" +
	"</optional>" +

	"<optional>" +
		"<element name='Recipes' a:help='List of training recipes'>" +
			"<zeroOrMore>" +
				"<element a:help='Training Recipe'>" +
					"<anyName/>" +
					"<interleave>" +
						"<element name='Input' a:help='Input unit'>" +
							"<text/>" +
						"</element>" +
						"<element name='Output' a:help='Output unit'>" +
							"<text/>" +
						"</element>" +
					"</interleave>" +
				"</element>" +
			"</zeroOrMore>" +
		"</element>" +
	"</optional>";
	
Training.GetOutput = function(template, ent) {
	for (let recipe in this.template.Recipes) {
		let input = this.template.Recipes[recipe].Input;
		let output =  this.template.Recipes[recipe].Output;
		
		let Identity = Engine.QueryInterface(ent, IID_Identity);
		
		//Query syntax.
		if (input[0] == "$") {
			input = input.substr(1);
			let splits = input.split(".");
			
			if (splits[0] == "Identity") {
				
				splits = splits[1].split("=");
				
				if (Identity[splits[0]]() == splits[1]) {
					return output
				}
				
			}
			continue
		}
		
		input = input.replace("{native}", Identity.GetCiv());
		output = output.replace("{native}", Identity.GetCiv());
		
		print(input, " ", template, "\n")
		if (input == template && rts.GetTemplate(output)) return output;
	}
	return null;
}

Training.Train = function(recipe) {
	
	let template_name = recipe[1];
	let ent = recipe[0];
	
	let Identity = Engine.QueryInterface(this.entity, IID_Identity);
	
	template_name = template_name.replace("{civ}", Identity.GetCiv());
	template_name = template_name.replace("{native}", Identity.GetCiv());
	
	let template = rts.GetTemplate(template_name);
	
	if (!template) {
		return;
	}
	
	//Check if the building has the required resources.
	let Dropsite = this.Get(IID_ResourceDropsite);
	
	//Get the costs.
	let costs = {};
	for (let res in template.Cost.Resources)
	{
		if (res == "food") continue; //Ignore food costs.
		
		if (+template.Cost.Resources[res] > 0) {
			costs[res] = +template.Cost.Resources[res];
		
			if (costs[res] > 0 && !Dropsite.Has(res, costs[res])) {
				this.Get(IID_GarrisonHolder).Eject(ent);
				rts.Notify(this.GetOwner(), "You do not have enough "+res+" to train this unit, consider supplying this building!");
				return;
			}
			
		}
	}
	
	for (let res in costs) {
		Dropsite.Take(res, costs[res]);
	}
	
	
	let new_ent = this.Spawn(template_name)
	if (new_ent) { 
		this.Get(IID_GarrisonHolder).PerformEject([ent], false);
		
		//Copy stuff over.
		rts.Copy(IID_Citizen, "name", ent, new_ent)
		rts.Copy(IID_Citizen, "home", ent, new_ent)
		rts.Copy(IID_Citizen, "hometown", ent, new_ent)
		
		rts.Copy(IID_Emotional, "religion", ent, new_ent)
		rts.Copy(IID_Emotional, "aspiration", ent, new_ent)
		rts.Copy(IID_Emotional, "likes", ent, new_ent)
		rts.Copy(IID_Emotional, "dislikes", ent, new_ent)
		rts.Copy(IID_Emotional, "star", ent, new_ent)
		rts.Copy(IID_ResourceGatherer, "carrying", ent, new_ent)
		
		//Update home.
		let House = rts.Call(IID_Citizen, ent).GetHouse();
		if (House) House.Rename(ent, new_ent);
		
		//Religous rituals.
		if (this.Get(IID_Temple)) {
			let cmp = rts.Call(IID_Emotional, new_ent);
			if (cmp) cmp.religion = this.Get(IID_Temple).GetReligion();
			rts.Get(IID_HintsManager).ReligionHint(this.GetOwner(), this.Get(IID_Temple).GetReligion());
			
			rts.Copy(IID_Temple, "religion", this.entity, new_ent)
		}

		if (this.template.Job) {
			rts.Call(IID_Citizen, new_ent).job = this.template.Job;
			Engine.PostMessage(new_ent, MT_Job, { to: this.template.Job });
		}
		
		Engine.DestroyEntity(ent);
	} else {
		rts.Notify(this.GetOwner(), "Could not train unit!");
	}
}

Training.OnGarrisonedUnitsChanged = function (msg) {
	if (msg.added.length > 0) {
		for (let entity of msg.added) {
			let output = this.GetOutput(rts.GetTemplateName(entity), entity);
			if (output) {
				this.NewTimer().Run("Train", [entity, output]).After(1000).Start();
			}
		}
	}
}

rts.RegisterComponent(IID_Training, "Training", Training)
