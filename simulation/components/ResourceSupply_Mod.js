ResourceSupply.prototype.Init = function()
{
	// Current resource amount (non-negative)
	this.amount = this.GetMaxAmount();

	// List of IDs for each player
	this.gatherers = [];
	let numPlayers = Engine.QueryInterface(SYSTEM_ENTITY, IID_PlayerManager).GetNumPlayers()
	for (let i = 0; i < numPlayers; ++i)
		this.gatherers.push([]);

	this.infinite = !isFinite(+this.template.Amount);

	
	if (this.template.Type == "vegetables.random") {
		
		this.cachedType = pickRandom([
			{"generic": "onions", "specific": "vegetable"},
			{"generic": "cabbage", "specific": "vegetable"},
			{"generic": "carrots", "specific": "vegetable"},
			{"generic": "cucumbers", "specific": "vegetable"},
			{"generic": "brocolli", "specific": "vegetable"},
			{"generic": "eggplant", "specific": "fruit"},
			{"generic": "corn", "specific": "vegetable"},
			{"generic": "tomatoes", "specific": "fruit"},
			{"generic": "potatoes", "specific": "vegetable"},
			{"generic": "cauliflowers", "specific": "vegetable"}
		])
		
		this.max_cache = Resources.GetResource(this.cachedType.generic).cache;
		this.charge_time = Resources.GetResource(this.cachedType.generic).recharge;
		
		
	} else {
		let [type, subtype] = this.template.Type.split('.');
		this.cachedType = { "generic": type, "specific": subtype };
		
		if (this.template.Cache) this.max_cache = +this.template.Cache;
		if (this.template.RechargeTime) this.charge_time =  +this.template.RechargeTime;
	}
	
	
	this.cache = 0;
	
	
	this.timer = null;
	this.recharging = false;
};

ResourceSupply.prototype.OnCreate = function()
{
	if (this.cachedType.generic == "stone") {
		let Pot = rts.Get(IID_ResourcePot);
		if (Pot.CountingStone) {
			Pot.Add("metal", 0.1*this.GetMaxAmount());
		}
	}
	
	if (this.template.StartFull) {
		this.cache = this.max_cache;
	}
};

ResourceSupply.prototype.Recharge = function() {
	this.recharging = false;
	this.cache = this.max_cache;
	
	let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
	if (StatusBars && StatusBars.enabled)
		StatusBars.RegenerateSprites();
	
	let type = this.GetType()
	
	//Notify gatherers that we have food now!
	let Gatherers = rts.Find(IID_ResourceGatherer).Near(this).Radius(20).List();
	for (let gatherer of Gatherers) {
		let Unit = Engine.QueryInterface(gatherer.entity, IID_UnitAI);

		if (Unit.order) {
			if (Unit.order.type == "Gather" || Unit.order.type == "GatherNearPosition") {
				if (Unit.order.data.type.generic == type.generic) {
					Unit.FinishOrder();
					Unit.PushOrderFront("Gather", {target: this.entity, type: type});
					gatherer.protesting = false;
				}
			}
		}
		
		
		
	}
}

ResourceSupply.prototype.TakeResources = function(rate)
{
	// Before changing the amount, activate Fogging if necessary to hide changes
	let cmpFogging = Engine.QueryInterface(this.entity, IID_Fogging);
	if (cmpFogging)
		cmpFogging.Activate();

	if (this.recharging) {
		return { "amount": 0, "exhausted": false };
	}

	if (this.cache > 0) {
		this.cache -= rate;
		
		if (this.cache <= 0) {
			this.recharging = true;
			this.timer = new rts.Timer(this.entity, IID_ResourceSupply).Run("Recharge").After(1000*this.charge_time).Start()
			
			let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
			if (StatusBars && StatusBars.enabled)
				StatusBars.RegenerateSprites();
		}
		
		return { "amount": rate, "exhausted": false };
	}
	
	
	if (this.infinite)
		return { "amount": rate, "exhausted": false };

	// 'rate' should be a non-negative integer

	var old = this.amount;
	this.amount = Math.max(0, old - rate);
	var change = old - this.amount;

	// Remove entities that have been exhausted
	if (this.amount === 0) {
		if (this.cachedType.generic == "wood") {
			let ent = Engine.AddLocalEntity("falling|"+rts.GetTemplateName(this.entity));
			let Position = Engine.QueryInterface(this.entity, IID_Position);
			let pos = Position.GetPosition();
			let angle = Position.GetRotation();
			
			Engine.QueryInterface(ent, IID_Visual).SetActorSeed(Engine.QueryInterface(this.entity, IID_Visual).GetActorSeed());
			
			PlaySound("death", this.entity);
			
			Engine.QueryInterface(ent, IID_Position).JumpTo(pos.x, pos.z);
			Engine.QueryInterface(ent, IID_Position).SetYRotation(angle.y);
		}
		Engine.DestroyEntity(this.entity);
	}
	 
	//UGLY HARD CODED HACK FOR NOW, make stone and metal fade into the ground.
	if (this.cachedType.generic == "stone" || this.cachedType.generic == "metal") {
		let Position = Engine.QueryInterface(this.entity, IID_Position);
		Position.SetConstructionProgress(this.amount/this.GetMaxAmount());
	}

	Engine.PostMessage(this.entity, MT_ResourceSupplyChanged, { "from": old, "to": this.amount });

	return { "amount": change, "exhausted": (this.amount === 0) };
};

ResourceSupply.prototype.GetMaxAmount = function()
{
	if (this.max) return this.max;
	return +this.template.Amount;
};


rts.RegisterOverlay(function(entity, graphics) {
	let Supply = Engine.QueryInterface(entity, IID_ResourceSupply)
	if (!Supply) return;
	
	let type = Supply.GetType().generic;
	let icon = "art/textures/ui/session/icons/resources/"+type+".png";
	
	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 2;
	let yoffset = +StatusBars.template.HeightOffset;
	let height = 2;
	
	graphics.AddSprite(
		icon,
		{ "x": -width, "y": -height },
		{ "x": width, "y": height},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
	
	if (Supply.recharging) {
		graphics.AddSprite(
			"art/textures/ui/session/icons/blocked.png",
			{ "x": -width, "y": -height },
			{ "x": width, "y": height},
			
			{ "x": 0, "y": yoffset, "z": 0 },
			"255 255 255 255"
		);
	}
})
