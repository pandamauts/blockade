let LikesAndDislikes = {};

LikesAndDislikes.Schema = rts.Schema.System;

LikesAndDislikes.Init = function() {
	this.list = [];
	this.names = [];
}

LikesAndDislikes.Add = function(name, icon) {
	if (this.names.indexOf(name) == -1) {
		this.names.push(name);
		this.list.push({Name: name, Icon: icon});
	}
}

LikesAndDislikes.GetRandom = function() {
	return pickRandom(this.list);
}

rts.RegisterSystemComponent(IID_LikesAndDislikes, "LikesAndDislikes", LikesAndDislikes);
