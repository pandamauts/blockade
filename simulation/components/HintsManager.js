var HintsManager = {};

HintsManager.Init = function() {
	this.hinted = {};
}

HintsManager.Hint = function(player, entity, hint) {
	let TemplateName = rts.GetTemplateName(entity);
	
	if (hint == "WhenBuilt" && !(hint+TemplateName in this.hinted)) {
		let Hints = rts.Call(IID_Hints, entity);
		let Identity = rts.Call(IID_Identity, entity);
		if (Hints && Hints.template.WhenBuilt) {
			rts.Popup(player, Identity.template.GenericName, Hints.template.WhenBuilt, "stretched:session/portraits/"+Identity.template.Icon);
			this.hinted[hint+TemplateName] = true;
		}
	}
}

HintsManager.HungerHint = function(player) {
	if ( !("_HungerHint_" in this.hinted) ) {
		
		rts.Popup(player, "Your people are hungry!", "Your citizens need food, otherwise they will not work. Feed them by gathering food from Animals, Plants, or Farms, or by storing food in readily available locations.", "stretched:session/icons/starving.png");
		
		this.hinted["_HungerHint_"] = true;
	}
}

HintsManager.SicknessHint = function(player) {
	if ( !("_SicknessHint_" in this.hinted) ) {
		
		rts.Popup(player, "Your people are sick!", "Uh oh! One or more of your people are sick. Take them to a temple to heal them.", "stretched:session/icons/sick_bubble.png");
		
		this.hinted["_SicknessHint_"] = true;
	}
}

HintsManager.ReligionHint = function(player, religion) {
	if ( !("_ReligionHint_" in this.hinted) ) {
		
		let icon = rts.Get(IID_ReligionManager).GetReligion(religion).icon;
		rts.Popup(player, "New Religion!", "One of your citizens has become a member of "+religion, "stretched:session/icons/religions/"+icon);
		
		this.hinted["_ReligionHint_"] = true;
	}
}

HintsManager.StarHint = function(player, religion) {
	if ( !("_StarHint_" in this.hinted) ) {
		
		rts.Popup(player, "You got a Star!", "Gold Stars can be used to upgrade your buildings, and create wonders. Save up 100 Silver Stars to get a Gold Star", "stretched:session/icons/star.png");
		
		this.hinted["_StarHint_"] = true;
	}
}

HintsManager.OutOfResourcesHint = function(player, resource) {
	if ( !("_OutOfResourcesHint_"+resource in this.hinted) ) {
		
		rts.Popup(player, "Missing Resources", "You are missing the required resources for this construction project, try collecting more of this type of resource.", "stretched:session/icons/resources/"+resource+".png");
		
		this.hinted["_OutOfResourcesHint_"+resource] = true;
	}
}

rts.RegisterSystemComponent(IID_HintsManager, "HintsManager", HintsManager);
