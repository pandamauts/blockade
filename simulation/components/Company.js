let Company = {};

let SearchRange = 200;
let MinumumOrder = 1;

/*
 * Non-depo companies want to fill their stock.
 * 
 * When companies recieve a demand, their wishlist for required supplies increases.
 * Companies make orders that fulfill their wishlist.
 * 
 * Companies have a list of orders that require processing.
 * 
 * Companies keep track of their turnover per minute and use this data to 
 * make further orders.
 * 
 */

Company.Schema = "<a:help>Deals with the number of workers available to do building specific tasks</a:help>" +
	"<a:example>" +
		"<Workers>1</Workers>" +
		"<Demands>wood metal</Demands>" +
		"<Activities>" +

			"<Activity1>" +
				"<Produces>" +
					"<furniture>1</furniture>" +
				"</Produces>" +
				"<Consumes>" +
					"<wood>1</wood>" +
				"</Consumes>" +
			"</Activity1>" +

			"<Activity2>" +
				"<Produces>" +
					"<arrows>1</arrows>" +
				"</Produces>" +
				"<Consumes>" +
					"<metal>1</metal>" +
					"<wood>1</wood>" +
				"</Consumes>" +
			"</Activity2>" +

		"</Activities>" +
	"</a:example>" +
	
	"<optional>" +
		"<element name='Demands' a:help='List of resources this company wants to maximise its stock of'>" +
			"<text/>" +
		"</element>" +
	"</optional>" +
	
	"<optional>" +
		"<element name='Activities' a:help='List of business companies'>" +
			"<zeroOrMore>" +
				"<element a:help='Business Activity'>" +
					"<anyName/>" +
					"<interleave>" +
						"<optional>" +
							"<element name='Automate' a:help='Automate?'>" +
								"<text/>" +
							"</element>" +
						"</optional>" +
					
						"<element name='Produces' a:help='What this Activity produces'>" +
							Resources.BuildSchema("positiveDecimal") +
						"</element>" +
						"<element name='Consumes' a:help='What this Activity consumes'>" +
							Resources.BuildSchema("positiveDecimal") +
						"</element>" +
					"</interleave>" +
				"</element>" +
			"</zeroOrMore>" +
		"</element>" +
	"</optional>";
	
Company.Init = function() {	
	//New Stuff.
	this.Wishlist = {};
	this.PreparedOrders = [];
	
	this.VisuallyDemanding = true;
} 

Company.Stock = function() {
	return this.Get(IID_ResourceDropsite);
}

//Let our neighbours know that we demand from them.
Company.OnOwnershipChanged = function() {
	this.Advertise();
}

//Someone has ordered a product from us, we need to deliver it to them!
Company.OnDemand = function(products) {

	//TODO += this? some sort of numerical demand?
	
	let new_products = [];
	
	for (let product of products) {
		if (this.Wishlist[product]) continue;
		
		this.Wishlist[product] = true;
		new_products.push(product);
	}
	
	if (new_products.length > 0) {
		//Let my neighbours know about this!
		let Companies = rts.Find(IID_Company).Near(this).Radius(SearchRange).List();
		for (let company of Companies) {
			company.OnDemand(new_products)
		}
	}
}

Company.PrepareOrder = function(order) {
	
	let Stock = this.Stock();
	
	if (!this.template || (this.template && this.template.Activities)) {
		if (Stock.Has(order.product, order.amount)) {
			Stock.Take(order.product, order.amount);
			
			if (Resources.GetResource(order.product).food) {
				Stock.stored.food -= order.amount;
			}
			
			this.PreparedOrders.push(order);
			return;
		}
	}
	
// 	//Check if we can actually sell the order!
	if (this.template && this.template.Activities) {
		for (let name in this.template.Activities) {
			
			let activity = this.template.Activities[name];
			
			if (order.product in activity.Produces) {
				
				let consumes = activity.Consumes;
				
				for (let resource in consumes) {
					let amount = consumes[resource];
					if (!Stock.Has(resource, amount)) {
						
						//print(amount, "\n");
						//error("Not enough "+resource+" to supply "+order.product);
						
						return;
					}
				}
				
				for (let resource in consumes) {
					let amount = consumes[resource];
					Stock.Take(resource, amount);
					
					if (Resources.GetResource(resource).food) {
						Stock.stored.food -= amount;
					}
				}
				
				this.PreparedOrders.push(order);
				
				return;
			}
		}
	}
}

Company.Produce = function(supply) {
	let Stock = this.Stock();
	
	// 	//Check if we can actually sell the order!
	if (this.template && this.template.Activities) {
		for (let name in this.template.Activities) {
			
			let activity = this.template.Activities[name];
			if (!activity.Automate) continue;
			
			if (supply.product in activity.Consumes) {
					
				let consumes = activity.Consumes;
				
				if (consumes[supply.product] > 1) {
					error("Company automation type not-supported");
				}
				
				let produces = activity.Produces;
				if (Stock.Has(supply.product, 1)) {
					
					for (let resource in produces) {
						if (produces[resource] > 1) {
							error("Company automation type not-supported");
						}
						
						this.VisuallyDemanding = false;
						
						if (!Stock.stored[resource]) Stock.stored[resource] = 0;
						Stock.stored[resource] += Stock.stored[supply.product];
						Stock.stored[supply.product] = 0;
						
						let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
						if (StatusBars && StatusBars.enabled)
							StatusBars.RegenerateSprites();
						
						break;
					}
					
				}

				return;
			}
		}
	}
}

Company.TryToSellTo = function(company) {
	
	if (!company.template) {
		return;
	}
	 
	for (let order of this.PreparedOrders) {
		if (order.client == company.entity) return;
	}
	
	if (company.template.Demands) {
		
		
		//What do we want to order?
		// First check our constant demands & then our wishlist.
		let Demands = company.template.Demands.split(/\s+/);
		let Stock = company.Stock();
		
		company.OnDemand(Demands);
		
		for (let demand of Demands) {
			
			// If they don't have too much already.
			if ( !(demand in Stock.stored) || Stock.stored[demand] <= +Stock.template.Max ) {
				
				let OrderSize = MinumumOrder;
				
				if (demand in Stock.stored) {
					OrderSize = +Stock.template.Max - Stock.stored[demand];
				} else {
					OrderSize = 10;
				}
				
				if (OrderSize > 10) {
					OrderSize = 10;
				}
				
				//Don't overshoot our own stock amount!
				//There must be at least one?
				if (!this.Stock().Has(demand, OrderSize)) {
					OrderSize = this.Stock().stored[demand];
				}
				
				if (OrderSize == 0 || !OrderSize) {
					OrderSize = MinumumOrder;
				}

				this.PrepareOrder({
					client: company.entity,
					amount: OrderSize,
					product: demand,
				});
				
			}
		}
	} else {
		
		for (let resource in company.Wishlist) {
			
			let OrderSize = MinumumOrder;
			
			//Don't overshoot our own stock amount!
			//There must be at least one?
			if (!this.Stock().Has(resource, 10)) {
				OrderSize = this.Stock().stored[resource];
			}
			
			if (OrderSize == 0 || !OrderSize) {
				OrderSize = MinumumOrder;
			}
			
			this.PrepareOrder({
				client: company.entity,
				amount: MinumumOrder,
				product: company.Wishlist[resource],
			});
			
		}
		
	}
	
}

//Advertises this company's production capability to all nearby companies. 
Company.Advertise = function() {
	//Find nearby companies.
	let Companies = rts.Find(IID_Company).Near(this).Radius(SearchRange).List();
	for (let company of Companies) {
		//Give this company the chance to make orders from me.
		this.TryToSellTo(company);
	}
}

//Send a worker out to dispatch the order.
Company.Dispatch = function(order) {
	//TODO check if the order is still valid!
	//Don't just remove it though
	let Client = rts.Call(IID_ResourceDropsite, order.client);
	if (order.product in Client.stored && Client.stored[order.product] >= +Client.template.Max) {
		if (this.Stock().stored[order.product]) {
			this.Stock().stored[order.product] += order.amount;
		} else {
			this.Stock().stored[order.product] = order.amount;
		}
		
		if (Resources.GetResource(order.product).food) {
			this.Stock().stored.food += order.amount;
		}
		
		return;
	}
	
	let GarrisonHolder = this.Get(IID_GarrisonHolder);

	//Dispatch.
	let Worker = GarrisonHolder.entities[0];
	let Gatherer = rts.Call(IID_ResourceGatherer, Worker);
	Gatherer.carrying[order.product] = order.amount;
	Gatherer.lastCarriedType = {generic: order.product, specific: ""};
		
	GarrisonHolder.Eject(Worker);
	
	let Unit = rts.Call(IID_UnitAI, Worker);
	Unit.PushOrderFront("Garrison", { "target": this.entity, "force": false });
	Unit.PushOrderFront("ReturnResource", { "target": order.client, "force": false });
	
	Unit.SetDefaultAnimationVariant();
}

Company.HasFreeWorker = function() {
	let GarrisonHolder = this.Get(IID_GarrisonHolder);
	return GarrisonHolder.entities.length > 0 && 
		rts.Call(IID_ResourceGatherer, GarrisonHolder.entities[0]);
}

//Sent by ResourceDropsite component.
Company.OnSupply = function(supply) {
	if (supply) {
		this.Wishlist[supply.product] = false;
	} 
	
	this.Produce(supply);
	this.Advertise();
	
	while (this.PreparedOrders.length > 0) {
		if (!this.HasFreeWorker()) return;
		this.Dispatch(this.PreparedOrders.pop());
	}
}

Company.OnGarrisonedUnitsChanged = function() {
	if (this.template && this.template.Activities) {
		this.NewTimer().Run("OnSupply").After(1000).Start();
	}
}

//Debugging purposes.
Company.Information = function() {
	
	let Demands = "null";
	if (this.template) {
		Demands = this.template.Demands;
	}
	
	return {
		Wishlist: this.Wishlist,
		Orders: this.PreparedOrders,
		Stock: this.Stock().stored,
		Demands: Demands
	};
}

//User Interface.
Company.Commands = {};
Company.Commands.EjectAll = "EjectAll";

Company.EjectAll = function() {
	let GarrisonHolder = this.Get(IID_GarrisonHolder);
	GarrisonHolder.PerformEject(GarrisonHolder.entities, false);
}

Company.Information = function() {
	if (this.Get(IID_GarrisonHolder)) {
		return {
			HasWorkers: this.Get(IID_GarrisonHolder).entities.length > 0
		};
	}
	return {};
}

rts.RegisterOverlay(function(entity, graphics) {
	let company = Engine.QueryInterface(entity, IID_Company)
	if (!company || !company.VisuallyDemanding) return;
					
	if (company.template && company.template.Demands) {
		
		let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

		let width = 4;
		let yoffset = +StatusBars.template.HeightOffset+1;
		let height = 4;
		
		let Demands = company.template.Demands.split(/\s+/);
		let Stock = company.Stock();
		
		
		let renderable = [];
		for (let demand of Demands) {
			
			if (demand in Stock.stored && Stock.stored[demand] >= +Stock.template.Max) continue;
					
			renderable.push(demand);	
		}
		
		let i = -4*renderable.length + 4;
		for (let demand of renderable) {
			
			let icon = "art/textures/ui/session/icons/resources/"+demand+".png";
					
			graphics.AddSprite(
				"art/textures/ui/session/icons/placemark.png",
				{ "x": i + -width, "y": -height},
				{ "x": i + width, "y": height},
				
				{ "x": 0, "y": yoffset, "z": 0 },
				"255 255 255 255"
			);
			
			graphics.AddSprite(
				icon,
				{ "x": i + -width/2, "y": -height/2+0.35},
				{ "x": i + width/2, "y": height/2+0.35},
				
				{ "x": 0, "y": yoffset, "z": 0 },
				"255 255 255 255"
			);
			
			i += 8;
		}
	}
	
})
	
rts.RegisterComponent(IID_Company, "Company", Company);
