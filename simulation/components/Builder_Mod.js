/**
 * Build/repair the target entity. This should only be called after a successful range check.
 * It should be called at a rate of once per second.
 */
Builder.prototype.PerformBuilding = function(target)
{
	let rate = this.GetRate();
	
	let ResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);

	let cmpFoundation = Engine.QueryInterface(target, IID_Foundation);
	if (cmpFoundation)
	{
		
		//They need to be carrying the correct resources!
		
		let missing = [];
		for (let resource in cmpFoundation.costs) {
			
			if (cmpFoundation.costs[resource] > 0) {
				if (ResourceGatherer.carrying[resource] == null || ResourceGatherer.carrying[resource] <= 0) {
					
					//Check if we can scavenge?
					if (cmpFoundation.replacing) {
						let Cost = Engine.QueryInterface(cmpFoundation.replacing, IID_Cost);
						let Health = Engine.QueryInterface(cmpFoundation.replacing, IID_Health);
						if (Cost.template.Resources[resource] && Cost.template.Resources[resource] > 0 && Health.hitpoints > 0) continue;
					}
					
					missing.push(resource);
				
				}
			}
		}
		
		if (missing.length == 0) {
			
			for (let resource in cmpFoundation.costs) {
				if (cmpFoundation.costs[resource] > 0) {
					
					let scavenged = false;
					//Try scavenging resources.
					if (cmpFoundation.replacing) {
						let Cost = Engine.QueryInterface(cmpFoundation.replacing, IID_Cost);
						let Health = Engine.QueryInterface(cmpFoundation.replacing, IID_Health);
						if (Cost.template.Resources[resource] && Cost.template.Resources[resource] > 0 && Health.hitpoints > 0) {
							Health.hitpoints -= 10;
							scavenged = true;
							//TODO fix leak when health is invalid?? Maybe health should be reset on upgrade! Is that a possible cheating oppurtunity?
						}
					}
					
					if (!scavenged) {
						ResourceGatherer.carrying[resource]--;
						if (ResourceGatherer.carrying[resource] == 0) {
							delete ResourceGatherer.carrying[resource];
							
							//TODO deal with more complicated cases.
							this.lastCarriedType = null;
							
							let UnitAI = Engine.QueryInterface(this.entity, IID_UnitAI);
							UnitAI.SetDefaultAnimationVariant();
						}
					}
					
					cmpFoundation.Build(this.entity, 10);
				}
			}
			
		} else {
			
			let nearby;
			
			for (let i = 0; i < missing.length; i++) {
				let resource = missing[i];
				
				let UnitAI = Engine.QueryInterface(this.entity, IID_UnitAI);
				
				//Give them credits.
				nearby = UnitAI.FindNearestDropsite("credit."+resource);
				if (nearby)
				{
					
					//Give them the actual credits bro.
					let ResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
					ResourceGatherer.carrying["credit."+resource] = 10;
					
					
					UnitAI.SetDefaultAnimationVariant();
					
					// (Keep the Build order on the stack so we'll
					// continue building after returning)
					UnitAI.PushOrderFront("ReturnResource", { "target": nearby, "force": false });

					// Oh no, couldn't find any drop sites. Give up on building.
					//UnitAI.FinishOrder();
					
				} else {
					
					//Maybe rewrite as a message?
					
					//I NEED A CERTAIN RESOURCE!
					let ResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
					
					let Ownership = Engine.QueryInterface(this.entity, IID_Ownership);
					if (Ownership) {
						rts.Get(IID_HintsManager).OutOfResourcesHint(Ownership.GetOwner(), resource);
					}
					ResourceGatherer.needs = resource;
					
					let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
					if (StatusBars && StatusBars.enabled)
						StatusBars.RegenerateSprites();

					UnitAI.PushOrderFront("Protest", {});
					return;
				}
				
			}
		}

		return
	}

	let cmpRepairable = Engine.QueryInterface(target, IID_Repairable);
	if (cmpRepairable)
	{
		cmpRepairable.Repair(this.entity, rate);
		return;
	}
}; 
