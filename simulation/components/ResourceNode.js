let ResourceNode = {}; 

ResourceNode.Schema = rts.Schema.Empty;

ResourceNode.Init = function() {
	this.peers = [];

	this.local_resources = {};
	this.local_producers = {};

	this.shared_resources = {};
};

ResourceNode.Has = function(resource) {
	return (resource in this.shared_resources &&  this.shared_resources[resource] > 0)
};

ResourceNode.Propagate = function(resource, amount, consume) {
	let peers = this.peers;
	
	let difference = 0;
	if (resource in this.shared_resources) {
		if (this.shared_resources[resource] == amount) return consume;
		difference = this.shared_resources[resource]-amount;
	}
	
	if (consume && difference > 0 && resource in this.local_resources && this.local_resources[resource] >= difference) {
		this.local_resources[resource] -= difference;
		consume = false;
	}
	this.shared_resources[resource] = amount;
	
	//Propagate to peers.
	for (let peer of peers) {
		let other = rts.Call(peer, IID_ResourceNode);
		
		if (!other) error("Missing resource node");
		
		consume = other.Propagate(resource, amount, consume);
	}
	
	this.RefreshStatusBars();
	
	return consume;
};

//Increase the resource count of a specific resource within the ResourceNetwork.
ResourceNode.Increase = function(resource) {
	
	//Increase local count.
	if (resource in this.local_resources) {
		this.local_resources[resource]++;
	} else {
		this.local_resources[resource] = 1;
	}
	
	if (!(resource in this.shared_resources)) {
		this.shared_resources[resource] = 0;
	}
	this.Propagate(resource, this.shared_resources[resource]+1);
};

//Increase the resource count of a specific resource within the ResourceNetwork.
ResourceNode.Decrease = function(resource) {
	this.Propagate(resource, this.shared_resources[resource]-1, true);
};

ResourceNode.Information = function() {
	return {
		SharedResources: this.shared_resources,
	};
}

rts.RegisterOverlay(function(entity, graphics) {
	let dropsite = Engine.QueryInterface(entity, IID_ResourceNode)
	if (!dropsite) return;
	
	let renderable = [];
	for (let resource in dropsite.shared_resources) {
		if (dropsite.shared_resources[resource] <= 0) continue; 
		if (resource == "food") continue;
		renderable.push(resource);
	}
	
	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 4;
	let yoffset = +StatusBars.template.HeightOffset-4;
	let height = 4;
	
	let i = -4*renderable.length + 4;
	for (let demand of renderable) {
		
		let icon = "art/textures/ui/session/icons/resources/"+demand+".png";
		
		graphics.AddSprite(
			icon,
			{ "x": i + -width/2, "y": -height/2+0.35},
			{ "x": i + width/2, "y": height/2+0.35},
			
			{ "x": 0, "y": yoffset, "z": 0 },
			"255 255 255 255"
		);
		
		i += 8;
	}
	
})

rts.RegisterComponent(IID_ResourceNode, "ResourceNode", ResourceNode);
