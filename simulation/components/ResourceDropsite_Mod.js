ResourceDropsite.prototype.Schema =
	"<element name='Types'>" +
		"<list>" +
			"<zeroOrMore>" +
				Resources.BuildChoicesSchema() +
			"</zeroOrMore>" +
		"</list>" +
	"</element>" +
	"<optional>" +
		"<element name='Max' a:help='The max stored of each resource'>" +
			"<data type='nonNegativeInteger'/>" +
		"</element>" +
	"<optional>" +
	"<optional>" +
		"<element name='Mixed' a:help='Is the max number distributed?'>" +
			"<data type='boolean'/>" +
		"</element>"+
	"</optional>" +
	"<element name='Sharable' a:help='Allows allies to use this entity.'>" +
		"<data type='boolean'/>" +
	"</element>";

ResourceDropsite.prototype.Init = function()
{
	this.sharable = this.template.Sharable == "true";
	this.shared = this.sharable;
  
  	//Blockade Mod.
	this.stored = {food: 0};
};


/**
 * Used to instantly give resources to dropsite.
 * @param resources The same structure as returned form GetCarryingStatus
 */
ResourceDropsite.prototype.GiveResource = function(resource)
{
	
	
  	if (resource.type in this.stored) {
		
		if (this.template.Max && Resources.GetResource(resource.type).food && 
			this.stored.food >= this.template.Max) {
			return;
		}
		
		this.stored[resource.type] += (resource.amount);
    } else {
     	this.stored[resource.type] = (resource.amount);
    }
    
	if (Resources.GetResource(resource.type).food) this.stored.food += (resource.amount);
    
	let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
	if (StatusBars && StatusBars.enabled)
		StatusBars.RegenerateSprites();
	
   Engine.PostMessage(this.entity, MT_Supply, {
		product: resource.type,
   });
};

/**
 * Returns data about what resources the unit is currently carrying,
 * in the form [ {"type":"wood", "amount":7, "max":10} ]
 */
ResourceDropsite.prototype.GetCarryingStatus = function()
{
	let ret = [];
	for (let type in this.stored)
	{
		ret.push({
			"type": type,
			"amount": this.stored[type],
			"max": 0
		});
	}
	return ret;
};

ResourceDropsite.prototype.HasFood = function() {
	return this.stored.food > 0
}

ResourceDropsite.prototype.EatFood = function() {
	
	for (let resource in this.stored) {
		let time = Resources.GetResource(resource).food;
		if (time && this.stored[resource] > 0) {
			this.stored[resource] -= 1;
			this.stored.food -= 1;
			
			//COMPANY HACK
			if (this.stored.food <=0) {
				let company = Engine.QueryInterface(this.entity, IID_Company);
				if (company) {
					company.VisuallyDemanding = true;
				}
			}
			
			let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
			if (StatusBars && StatusBars.enabled)
				StatusBars.RegenerateSprites();
			
			return time*1000;
		}
	}
	
	
	error("Cannot eat non-existing food")
	return 3
}

ResourceDropsite.prototype.Has = function(resource, amount) {
	let node = rts.Call(IID_ResourceNode, this.entity);
	if (!node) return false;

	return node.Has(resource);
}

ResourceDropsite.prototype.Take = function(resource, amount) {
	this.stored[resource] -= amount;
	
	let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
	if (StatusBars && StatusBars.enabled)
		StatusBars.RegenerateSprites();
}

ResourceDropsite.prototype.Total = function() {
	let sum = 0;
	for (let amount of this.stored) {
		sum += amount;
	}
	if ("food" in this.stored) {
		sum -= this.stored["food"];
	}
	return sum;
}

/**
 * Returns whether this dropsite accepts the given generic type of resource.
 */
ResourceDropsite.prototype.AcceptsType = function(type)
{
	//If resource is null, of course we can return it!
	if (type == undefined) return true;
	
	if (type != type.replace("credit.", "")) {
		//Ignore credits.
		type = type.replace("credit.", "");
		
		return this.Has(type)
	}
	
	if (this.Mixed) {
		if (this.template.Max && !(Resources.GetResource(type).food) && type in this.stored && this.Total() >= +this.template.Max) return false;
	} else {
		if (this.template.Max && !(Resources.GetResource(type).food) && type in this.stored && this.stored[type] >= +this.template.Max) return false;
	}
	
	if (this.GetTypes().indexOf("food") != -1 && Resources.GetResource(type).food) {
		return true;
	}

	return this.GetTypes().indexOf(type) != -1;
};

rts.RegisterOverlay(function(entity, graphics) {
	let dropsite = Engine.QueryInterface(entity, IID_ResourceDropsite)
	if (!dropsite) return;
	
	let renderable = [];
	for (let resource in dropsite.stored) {
		if (dropsite.stored[resource] <= 0) continue; 
		if (resource == "food") continue;
		renderable.push(resource);
	}
	
	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 4;
	let yoffset = +StatusBars.template.HeightOffset-4;
	let height = 4;
	
	let i = -4*renderable.length + 4;
	for (let demand of renderable) {
		
		let icon = "art/textures/ui/session/icons/resources/"+demand+".png";
		
		graphics.AddSprite(
			icon,
			{ "x": i + -width/2, "y": -height/2+0.35},
			{ "x": i + width/2, "y": height/2+0.35},
			
			{ "x": 0, "y": yoffset, "z": 0 },
			"255 255 255 255"
		);
		
		i += 8;
	}
	
})
