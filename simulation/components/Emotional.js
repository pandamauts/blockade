
//Happiness factors.

	const Full = 0;
	const Calm = 1;
	const Accommodation = 2
	const Community = 3;
	const Healthy = 4;
	const Entertained = 5;
	
	const DreamJob = 6
	const Spoilt = 7
	const Relaxed = 8
	
	const LoyaltyThreshold = 2;
	const StarThreshold = 7;

let Emotional = {};

let Aspirations = [
	"Farmer",
	"Hunter",
	"Blacksmith",
	"Miner",
	"Druid",
	"Builder",
	"Lumberer",
	"Soldier",
	"Captain",
	"Trader",
];

Emotional.Init = function() {
	this.levels = new Array(10);
	this.owner = -1;
	this.dissenting = false;
	this.likes = "";
	
	this.sick = false;
}

Emotional.OnCreate = function() {
	this.levels[Full] = 1;
	this.levels[Calm] = 1;
	this.levels[Healthy] = 1;
	this.levels[Accommodation] = 1;
	this.levels[Community] = 0;
	this.levels[Entertained] = 0;
	this.levels[DreamJob] = 0;
	this.levels[Spoilt] = 0;
	this.levels[Relaxed] = 0;

	this.aspiration = pickRandom(Aspirations);
	this.likes = rts.Get(IID_LikesAndDislikes).GetRandom();
	this.dislikes = rts.Get(IID_LikesAndDislikes).GetRandom();
	
	this.religion = pickRandom(rts.Get(IID_ReligionManager).None);
	
	this.neighbors = [];
}

Emotional.GetAspiration = function() {
	let belief = rts.Get(IID_ReligionManager).GetReligion(this.religion);
	if (belief && belief.aspiration) {
		return belief.aspiration;
	}
	return this.aspiration;
}

Emotional.GetReligionIcon = function() {
	let belief = rts.Get(IID_ReligionManager).GetReligion(this.religion);
	if (belief && belief.icon) {
		return belief.icon;
	}
	return "0.png";
}

Emotional.GetLike = function() {
	let belief = rts.Get(IID_ReligionManager).GetReligion(this.religion);
	if (belief && belief.likes) {
		return belief.likes;
	}
	return this.likes;
}

Emotional.GetDislike = function() {
	let belief = rts.Get(IID_ReligionManager).GetReligion(this.religion);
	if (belief && belief.dislikes) {
		return belief.dislikes;
	}
	return this.dislikes;
}

Emotional.OnOwnershipChanged = function(msg) {
	if (msg.to != INVALID_PLAYER) {
		rts.Signal(this).When(IID_Identity).Within(20).Range();
	}
}

Emotional.OnSignal = function(msg) {
	
	if (msg.added.length > 0)
		for (let entity of msg.added)
			this.neighbors.push(entity);

	if (msg.removed.length > 0)
		for (let entity of msg.removed)
			this.neighbors.splice(this.neighbors.indexOf(entity), 1);
	
	this.levels[Spoilt] = 0;
	this.levels[Relaxed] = 0;
	
	let like = this.GetLike().Name;
	let dislike = this.GetDislike().Name;
		
	for (let ent of this.neighbors) {
		let Identity = rts.Call(IID_Identity, ent);
		if (Identity) {
			if (Identity.name == like) {
				this.levels[Spoilt] = 1;
			}
			if (Identity.name == dislike) {
				this.levels[Relaxed] = -1;
			}
		}
	}
}

Emotional.OnJob = function(msg) {
	let job = msg.to;
	
	if (job == this.GetAspiration()) {
		this.levels[DreamJob] = 1;
	} else {
		this.levels[DreamJob] = 0;
	}
}

Emotional.State = function() {
	let sum = 0;
	for (let i = 0; i < this.levels.length; i++) {
		if (this.levels[i]) sum += this.levels[i];
	}
	return sum;
}

Emotional.UpdateLoyalty = function() {
	let state = this.State();
	if (!this.dissenting && state <= LoyaltyThreshold) {
		
		//let Ownership = this.Get(IID_Ownership);
		
		//this.owner = this.GetOwner();
		this.dissenting = true;
		
		//Ownership.SetOwner(rts.Gaia);
		
	} else if (this.dissenting && state > LoyaltyThreshold) {
		
		this.dissenting = false;
		
		//let Ownership = this.Get(IID_Ownership);
		//Ownership.SetOwner(this.owner);

	}
	
	if (this.star) {
		return;
	}
	
	if (state >= StarThreshold) {
		let city = this.Get(IID_Citizen).GetCity();
		city.AddStar(this.entity);
		this.star = true;
		PlaySound("star", this.entity);
	}
}

Emotional.OnStarving = function() {
	if (!this.sick) {
		this.sick = true;
		
		let Health = this.Get(IID_Health);
		Health.Reduce(10);

		let Motion = this.Get(IID_UnitMotion);
		Motion.SetSpeed(Motion.GetWalkSpeed()/2);
	
		rts.Get(IID_HintsManager).SicknessHint(this.GetOwner());
	}
}


Emotional.OnHungry = function() {
	this.levels[Full] = 0;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnFull = function() {
	this.levels[Full] = 1;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnChaos = function() {
	this.levels[Calm] = 0;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnOrder = function() {
	this.levels[Calm] = 1;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnLonely = function() {
	this.levels[Community] = 0;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnCommunity = function() {
	this.levels[Community] = 1;
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.OnHealthChanged = function(msg) {
	if (msg.from < msg.to) {
		
		//Convert to a nearby religion?
		let temples = rts.Find(IID_Temple).Near(this).List();
		if (temples.length > 0) {
			//TODO refactor this into a convert method.
			this.religion = temples[0].GetReligion();
			rts.Get(IID_HintsManager).ReligionHint(this.GetOwner(), temples[0].GetReligion());
		}
		
		//Not SICK ANYMORE.
		//Thank God.
		if (this.sick) {
			this.sick = false;
			let Motion = this.Get(IID_UnitMotion);
			Motion.SetSpeed(Motion.GetWalkSpeed());
		}
		
		this.levels[Healthy] = 1;
	}
	if (msg.from > msg.to) {
		this.levels[Healthy] = 0;
	}
	this.RefreshStatusBars();
	this.UpdateLoyalty();
}

Emotional.Icon = function() {
	let state = this.State();
	
	let icon;
	
	if (this.sick && state > LoyaltyThreshold) {
		return "sick.png"
	}

	switch (state) {
		case 0:
		case 1:
			return "angry.png";
		case 2:
			return "verysad.png";
		case 3:
			return "sad.png";
		case 4:
			return "calm.png";
		case 5:
			return "happy.png";
		case 6:
			return "veryhappy.png";
		case 7:
			this.UpdateLoyalty();
		case 8:
			return "loyal.png";
	}
}

Emotional.Information = function() {
	let state = this.State();
	let icon = this.Icon();
	
	return {
		State: state,
		Icon: icon,
		Aspiration: this.GetAspiration(),
		Likes: this.GetLike(),
		Sick: this.sick,
		Dislikes: this.GetDislike(),
		Religion: this.religion,
		ReligionIcon: this.GetReligionIcon(),
		
		Star: this.star,
	}
}

rts.RegisterOverlay(function(entity, graphics) {
	let Emotional = Engine.QueryInterface(entity, IID_Emotional)
	if (!Emotional) return;
	if (!Emotional.sick) return;

	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 1.5;
	let yoffset = +StatusBars.template.HeightOffset+1;
	let height = 1.5;
	
	graphics.AddSprite(
		"art/textures/ui/session/icons/sick_bubble.png",
		{ "x": -width+0.9, "y": -height},
		{ "x": width+0.9, "y": height},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
})

rts.RegisterOverlay(function(entity, graphics) {
	let Hunger = Engine.QueryInterface(entity, IID_Hunger)
	if (!Hunger || !Hunger.hungry) return;

	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 1.5;
	let yoffset = +StatusBars.template.HeightOffset+1;
	let height = 1.5;
	
	graphics.AddSprite(
		"art/textures/ui/session/icons/starving.png",
		{ "x": -width+0.9, "y": -height},
		{ "x": width+0.9, "y": height},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
})

rts.RegisterOverlay(function(entity, graphics) {
	let Emotional = Engine.QueryInterface(entity, IID_Emotional)
	if (!Emotional) return;
	if (Emotional.State() > LoyaltyThreshold) return;

	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 1.5;
	let yoffset = +StatusBars.template.HeightOffset+1;
	let height = 1.5;
	
	let icon = "art/textures/ui/session/icons/riot.png";
	
	graphics.AddSprite(
		"art/textures/ui/session/icons/speech_bubble.png",
		{ "x": -width+0.9, "y": -height},
		{ "x": width+0.9, "y": height},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
		
	graphics.AddSprite(
		icon,
		{ "x": -width+0.9, "y": -height+0.35},
		{ "x": width+0.9, "y": height+0.35},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
})

rts.RegisterOverlay(function(entity, graphics) {
	let ResourceGatherer = Engine.QueryInterface(entity, IID_ResourceGatherer)
	if (!ResourceGatherer) return;
					
	let needs = ResourceGatherer.needs;
	
	if (needs) {

		let icon = "art/textures/ui/session/icons/resources/"+needs+".png";
		
		let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

		let width = 1.5;
		let yoffset = +StatusBars.template.HeightOffset+1;
		let height = 1.5;
		
		graphics.AddSprite(
			"art/textures/ui/session/icons/speech_bubble.png",
			{ "x": -width+0.9, "y": -height},
			{ "x": width+0.9, "y": height},
			
			{ "x": 0, "y": yoffset, "z": 0 },
			"255 255 255 255"
		);
		
		graphics.AddSprite(
			icon,
			{ "x": -width+0.9, "y": -height+0.35},
			{ "x": width+0.9, "y": height+0.35},
			
			{ "x": 0, "y": yoffset, "z": 0 },
			"255 255 255 255"
		);
	}
	
})


/*rts.RegisterOverlay(function(entity, graphics) {
	let Emotional = Engine.QueryInterface(entity, IID_Emotional)
	if (!Emotional) return;
	
	let state = Emotional.State();
	let icon = "art/textures/ui/session/icons/"+Emotional.Icon();
	let StatusBars = Engine.QueryInterface(entity, IID_StatusBars);

	let width = 1;
	let yoffset = +StatusBars.template.HeightOffset+1;
	let height = 1;
	
	graphics.AddSprite(
		icon,
		{ "x": -width, "y": -height },
		{ "x": width, "y": height},
		
		{ "x": 0, "y": yoffset, "z": 0 },
		"255 255 255 255"
	);
})*/

rts.RegisterComponent(IID_Emotional, "Emotional", Emotional);
