let Temple = {};

Temple.Init = function() {
	this.type = 0;
	this.value = "";
}

Temple.OnCreate = function() {
	this.religion = rts.Get(IID_ReligionManager).NewReligion();
}

Temple.GetReligion = function() {
	return this.religion
}

Temple.GetReligionIcon = function() {
	let belief = rts.Get(IID_ReligionManager).GetReligion(this.religion);
	if (belief && belief.icon) {
		return belief.icon;
	}
	return "0.png";
}

Temple.Information = function() {
	return {
		Religion: this.GetReligion(),
		Icon: this.GetReligionIcon(),
		More: rts.Get(IID_ReligionManager).GetReligion(this.religion)
	};
}

rts.RegisterComponent(IID_Temple, "Temple", Temple);
