let Upgrades = {};

Upgrades.Schema = 
	"<a:help>Allows the upgrading of buildings.</a:help>" +
	"<a:example>" +
		"<Entity>" +
			"structures/athens_house" +
		"</Entity>" +
	"</a:example>" +
	"<element name='Into' a:help='One of the entities this entity can upgrade to'>" +
		"<text/>" +
	"</element>";
				
Upgrades.Init = function() {
	this.into = this.template.Into.split(/\s+/);
	this.upgrade = null;
}

Upgrades.Upgrade = function(data) {
	let Cities = rts.Find(IID_CityState).OwnedBy(this.GetOwner()).Near(this).List();
	if (!Cities || Cities.length == 0) return;
	
	let City = Cities[0];
	
	if (!City.SpendStar()) {
		//Not enough stars!
		return;
	}
	
	let name = this.into[data.Into];
	
	let Identity = Engine.QueryInterface(this.entity, IID_Identity);
	
	name = name.replace("{civ}", Identity.GetCiv());
	name = name.replace("{native}", Identity.GetCiv());
	
	this.upgrade = this.SpawnFoundation(this.into[data.Into]);
	
	rts.Call(IID_Foundation, this.upgrade).Replace(this.entity);
}

Upgrades.Information = function() {
	return {
		Into: this.into,
	};
}

Upgrades.Commands = {};
Upgrades.Commands.Upgrade = "Upgrade";

rts.RegisterComponent(IID_Upgrades, "Upgrades", Upgrades);
