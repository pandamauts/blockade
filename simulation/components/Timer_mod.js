/**
 * Cancels an existing timer that was created with SetTimeout/SetInterval.
 */
Timer.prototype.UpdateTimer = function(id, repeatTime)
{
	this.timers.get(id).repeatTime = repeatTime;
}; 
