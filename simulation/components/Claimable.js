let Claimable = {};

Claimable.OnOwnershipChanged = function(msg) {
	if (msg.to != INVALID_PLAYER) {
		rts.Signal(this).When(IID_Identity).Within(10).Range();
	}
}

Claimable.OnSignal = function(msg) {
	if (msg.added && msg.added.length > 0) {
		
		this.Get(IID_Ownership).SetOwner(rts.Call(IID_Ownership, msg.added[0]).GetOwner());
		
	}
}

rts.RegisterComponent(IID_Claimable, "Claimable", Claimable);
