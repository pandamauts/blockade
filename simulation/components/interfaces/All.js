Engine.RegisterInterface("AnimalPen");  
Engine.RegisterInterface("Claimable");  
Engine.RegisterInterface("Upgrades");  
Engine.RegisterInterface("Builds");  
Engine.RegisterInterface("Training"); 
Engine.RegisterInterface("Rewards");  
Engine.RegisterInterface("School");  
Engine.RegisterInterface("LikesAndDislikes");

Engine.RegisterInterface("ReligionManager");  
Engine.RegisterInterface("Temple"); 

Engine.RegisterInterface("Hints");
Engine.RegisterInterface("HintsManager");

Engine.RegisterInterface("ResourceNode");

Engine.RegisterInterface("Company");  
	Engine.RegisterMessageType("Supply"); // {product: "wood", amount: 1}
	Engine.RegisterMessageType("Demand"); // {client: id, product: "wood", amount: 1, chain: [id1, id2, id3]}


