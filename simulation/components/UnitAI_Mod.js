UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.GATHER.GATHERING.enter = function() {
	this.gatheringTarget = this.order.data.target;	// deleted in "leave".

	// Check if the resource is full.
	if (this.gatheringTarget)
	{
		// Check that we can gather from the resource we're supposed to gather from.
		// Will only be added if we're not already in.
		var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
		var cmpSupply = Engine.QueryInterface(this.gatheringTarget, IID_ResourceSupply);
		if (!cmpSupply || !cmpSupply.AddGatherer(cmpOwnership.GetOwner(), this.entity))
		{
			this.gatheringTarget = INVALID_ENTITY;
			this.StartTimer(0);
			return false;
		}
	}

	// If this order was forced, the player probably gave it, but now we've reached the target
	//	switch to an unforced order (can be interrupted by attacks)
	this.order.data.force = false;
	this.order.data.autoharvest = true;

	// Calculate timing based on gather rates
	// This allows the gather rate to control how often we gather, instead of how much.
	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	var rate = cmpResourceGatherer.GetTargetGatherRate(this.gatheringTarget);

	if (!rate)
	{
		// Try to find another target if the current one stopped existing
		if (!Engine.QueryInterface(this.gatheringTarget, IID_Identity))
		{
			// Let the Timer logic handle this
			this.StartTimer(0);
			return false;
		}

		// No rate, give up on gathering
		this.FinishOrder();
		return true;
	}

	// Scale timing interval based on rate, and start timer
	// The offset should be at least as long as the repeat time so we use the same value for both.
	var offset = 1000/rate;
	var repeat = offset;
	this.StartTimer(offset, repeat);

	// We want to start the gather animation as soon as possible,
	// but only if we're actually at the target and it's still alive
	// (else it'll look like we're chopping empty air).
	// (If it's not alive, the Timer handler will deal with sending us
	// off to a different target.)
	if (this.CheckTargetRange(this.gatheringTarget, IID_ResourceGatherer))
	{
		this.SetDefaultAnimationVariant();
		var typename = Resources.GetResource(this.order.data.type.generic).gatherAnimation;
		if (!typename) {
			typename ="gather_" + this.order.data.type.specific;
		}
		this.SelectAnimation(typename);
	}
	return false;
}

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.GATHER.GATHERING.Timer = function(msg) {
	var resourceTemplate = this.order.data.template;
	var resourceType = this.order.data.type;

	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	if (!cmpOwnership)
		return;

	var cmpSupply = Engine.QueryInterface(this.gatheringTarget, IID_ResourceSupply);
	if (cmpSupply && cmpSupply.IsAvailable(cmpOwnership.GetOwner(), this.entity))
	{
		// Check we can still reach and gather from the target
		if (this.CheckTargetRange(this.gatheringTarget, IID_ResourceGatherer) && this.CanGather(this.gatheringTarget))
		{
			// Gather the resources:

			var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);

			// Try to gather treasure
			if (cmpResourceGatherer.TryInstantGather(this.gatheringTarget))
				return;

			// If we've already got some resources but they're the wrong type,
			// drop them first to ensure we're only ever carrying one type
			//if (cmpResourceGatherer.IsCarryingAnythingExcept(resourceType.generic))
				//cmpResourceGatherer.DropResources();

			// Collect from the target
			var status = cmpResourceGatherer.PerformGather(this.gatheringTarget);

			// If we've collected as many resources as possible,
			// return to the nearest dropsite
			if (status.filled)
			{
				var nearby = this.FindNearestDropsite(resourceType.generic);
				if (nearby)
				{
					// (Keep this Gather order on the stack so we'll
					// continue gathering after returning)
					this.PushOrderFront("ReturnResource", { "target": nearby, "force": false });
					return;
				}

				// Oh no, couldn't find any drop sites. Give up on gathering.
				this.FinishOrder();
				return;
			}

			// We can gather more from this target, do so in the next timer
			if (!status.exhausted)
				return;
		}
		else
		{
			// Try to follow the target
			if (this.MoveToTargetRange(this.gatheringTarget, IID_ResourceGatherer))
			{
				this.SetNextState("APPROACHING");
				return;
			}

			// Can't reach the target, or it doesn't exist any more

			// We want to carry on gathering resources in the same area as
			// the old one. So try to get close to the old resource's
			// last known position

			var maxRange = 8; // get close but not too close
			if (this.order.data.lastPos &&
				this.MoveToPointRange(this.order.data.lastPos.x, this.order.data.lastPos.z,
					0, maxRange))
			{
				this.SetNextState("APPROACHING");
				return;
			}
		}
	}

	// We're already in range, can't get anywhere near it or the target is exhausted.

	var herdPos = this.order.data.initPos;

	// Give up on this order and try our next queued order
	// but first check what is our next order and, if needed, insert a returnResource order
	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	if (cmpResourceGatherer.IsCarrying(resourceType.generic) &&
		this.orderQueue.length > 1 && this.orderQueue[1] !== "ReturnResource" &&
		(this.orderQueue[1].type !== "Gather" || this.orderQueue[1].data.type.generic !== resourceType.generic))
	{
		let nearby = this.FindNearestDropsite(resourceType.generic);
		if (nearby)
			this.orderQueue.splice(1, 0, { "type": "ReturnResource", "data": { "target": nearby, "force": false } });
	}
	if (this.FinishOrder())
		return;

	// No remaining orders - pick a useful default behaviour

	// Try to find a new resource of the same specific type near our current position:
	// Also don't switch to a different type of huntable animal
	var nearby = this.FindNearbyResource(function(ent, type, template) {
		return (
			(type.generic == "treasure" && resourceType.generic == "treasure")
			|| (type.specific == resourceType.specific
			&& (type.specific != "meat" || resourceTemplate == template))
		);
	});
	if (nearby)
	{
		this.PerformGather(nearby, false, false);
		return;
	}

	// If hunting, try to go to the initial herd position to see if we are more lucky
	if (herdPos)
	{
		this.GatherNearPosition(herdPos.x, herdPos.z, resourceType, resourceTemplate);
		return;
	}

	// Nothing else to gather - if we're carrying anything then we should
	// drop it off, and if not then we might as well head to the dropsite
	// anyway because that's a nice enough place to congregate and idle

	var nearby = this.FindNearestDropsite(resourceType.generic);
	if (nearby)
	{
		this.PushOrderFront("ReturnResource", { "target": nearby, "force": false });
		return;
	}

	// No dropsites - just give up
};

UnitAI.prototype.UnitFsmSpec["Order.Repair"] = function(msg) {
	if (this.CheckTargetRange(this.order.data.target, IID_Builder)) {
		// We are already at the target, or can't move at all,
		// so try repairing it from here.
		// TODO: need better handling of the can't-reach-target case
		this.SetNextStateAlwaysEntering("INDIVIDUAL.REPAIR.REPAIRING");
		return;
	}
	
	// Try to move within range
	if (this.MoveToTargetRange(this.order.data.target, IID_Builder))
	{
		// We've started walking to the given point
		this.SetNextState("INDIVIDUAL.REPAIR.APPROACHING");
	}
}




UnitAI.prototype.UnitFsmSpec["Order.ReturnResource"] = function(msg) {
	// Check if the dropsite is already in range
	if (this.CheckTargetRange(this.order.data.target, IID_ResourceGatherer) && this.CanReturnResource(this.order.data.target, true))
	{
		var cmpResourceDropsite = Engine.QueryInterface(this.order.data.target, IID_ResourceDropsite);
		if (cmpResourceDropsite)
		{
			// Dump any resources we can
			var dropsiteTypes = cmpResourceDropsite.GetTypes();

			//Blockade Mod.
			Engine.QueryInterface(this.entity, IID_ResourceGatherer).CommitResources(cmpResourceDropsite);
			// Stop showing the carried resource animation.
			this.SetDefaultAnimationVariant();

			// Our next order should always be a Gather,
			// so just switch back to that order
			this.FinishOrder();
			return;
		}
	}
	// Try to move to the dropsite
	if (this.MoveToTargetRange(this.order.data.target, IID_ResourceGatherer))
	{
		// We've started walking to the target
		this.SetNextState("INDIVIDUAL.RETURNRESOURCE.APPROACHING");
		return;
	}
	// Oops, we can't reach the dropsite.
	// Maybe we should try to pick another dropsite, to find an
	// accessible one?
	// For now, just give up.
	this.StopMoving();
	this.FinishOrder();
	return;
};

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.RETURNRESOURCE.APPROACHING.MoveCompleted = function() {
	// Switch back to idle animation to guarantee we won't
	// get stuck with the carry animation after stopping moving
	this.SelectAnimation("idle");

	// Check the dropsite is in range and we can return our resource there
	// (we didn't get stopped before reaching it)
	if (this.CheckTargetRange(this.order.data.target, IID_ResourceGatherer) && this.CanReturnResource(this.order.data.target, true))
	{
		var cmpResourceDropsite = Engine.QueryInterface(this.order.data.target, IID_ResourceDropsite);
		if (cmpResourceDropsite)
		{
			// Dump any resources we can
			var dropsiteTypes = cmpResourceDropsite.GetTypes();

			var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
			
			//Blockade Mod
			cmpResourceGatherer.CommitResources(cmpResourceDropsite);

			// Stop showing the carried resource animation.
			this.SetDefaultAnimationVariant();

			// Our next order should always be a Gather,
			// so just switch back to that order
			this.FinishOrder();
			return;
		}
	}

	// The dropsite was destroyed, or we couldn't reach it, or ownership changed
	// Look for a new one.

	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	var genericType = cmpResourceGatherer.GetMainCarryingType();
	var nearby = this.FindNearestDropsite(genericType);
	if (nearby)
	{
		this.FinishOrder();
		this.PushOrderFront("ReturnResource", { "target": nearby, "force": false });
		return;
	}

	// Oh no, couldn't find any drop sites. Give up on returning.
	this.FinishOrder();
};

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.REPAIR.ConstructionFinished = function(msg) {
	if (msg.data.entity != this.order.data.target)
		return; // ignore other buildings

	// Save the current order's data in case we need it later
	var oldData = this.order.data;

	// Save the current state so we can continue walking if necessary
	// FinishOrder() below will switch to IDLE if there's no order, which sets the idle animation.
	// Idle animation while moving towards finished construction looks weird (ghosty).
	var oldState = this.GetCurrentState();

	// Drop any resource we can if we are in range when the construction finishes
	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	var cmpResourceDropsite = Engine.QueryInterface(msg.data.newentity, IID_ResourceDropsite);
	if (cmpResourceGatherer && cmpResourceDropsite && this.CheckTargetRange(msg.data.newentity, IID_Builder) &&
		this.CanReturnResource(msg.data.newentity, true))
	{
		let dropsiteTypes = cmpResourceDropsite.GetTypes();
		
		//Blockade Mod
		cmpResourceGatherer.CommitResources(cmpResourceDropsite);
		this.SetDefaultAnimationVariant();
	}

	// We finished building it.
	// Switch to the next order (if any)
	if (this.FinishOrder())
	{

		if (this.CanReturnResource(msg.data.newentity, true))
		{
			this.PushOrderFront("ReturnResource", { "target": msg.data.newentity, "force": false });
		}
		this.SetDefaultAnimationVariant();
		return;
	}

	// No remaining orders - pick a useful default behaviour

	// If autocontinue explicitly disabled (e.g. by AI) then
	// do nothing automatically
	if (!oldData.autocontinue)
		return;

	// If this building was e.g. a farm of ours, the entities that recieved
	// the build command should start gathering from it
	if ((oldData.force || oldData.autoharvest) && this.CanGather(msg.data.newentity))
	{
		if (this.CanReturnResource(msg.data.newentity, true))
		{
			this.SetDefaultAnimationVariant();
			this.PushOrder("ReturnResource", { "target": msg.data.newentity, "force": false });
		}
		this.PerformGather(msg.data.newentity, true, false);
		return;
	}

	// Unit was approaching and there's nothing to do now, so switch to walking
	if (oldState === "INDIVIDUAL.REPAIR.APPROACHING")
	{
		// We're already walking to the given point, so add this as a order.
		this.WalkToTarget(msg.data.newentity, true);
	}
};

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.enter = function() {};

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.GARRISON.GARRISONED.enter = function() {
	if (this.order.data.target)
		var target = this.order.data.target;
	else
	{
		this.FinishOrder();
		return true;
	}

	if (this.IsGarrisoned())
		return false;

	// Check that we can garrison here
	if (this.CanGarrison(target))
	{
		// Check that we're in range of the garrison target
		if (this.CheckGarrisonRange(target))
		{
			var cmpGarrisonHolder = Engine.QueryInterface(target, IID_GarrisonHolder);
			// Check that garrisoning succeeds
			if (cmpGarrisonHolder.Garrison(this.entity))
			{
				this.isGarrisoned = true;

				if (this.formationController)
				{
					var cmpFormation = Engine.QueryInterface(this.formationController, IID_Formation);
					if (cmpFormation)
					{
						// disable rearrange for this removal,
						// but enable it again for the next
						// move command
						var rearrange = cmpFormation.rearrange;
						cmpFormation.SetRearrange(false);
						cmpFormation.RemoveMembers([this.entity]);
						cmpFormation.SetRearrange(rearrange);
					}
				}

				// Check if we are garrisoned in a dropsite
				var cmpResourceDropsite = Engine.QueryInterface(target, IID_ResourceDropsite);
				if (cmpResourceDropsite && this.CanReturnResource(target, true))
				{
					// Dump any resources we can
					var dropsiteTypes = cmpResourceDropsite.GetTypes();
					var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
					if (cmpResourceGatherer)
					{
						//Blockade Mod
						cmpResourceGatherer.CommitResources(cmpResourceDropsite);
						this.SetDefaultAnimationVariant();
					}
				}

				// If a pickup has been requested, remove it
				if (this.pickup)
				{
					var cmpHolderPosition = Engine.QueryInterface(target, IID_Position);
					var cmpHolderUnitAI = Engine.QueryInterface(target, IID_UnitAI);
					if (cmpHolderUnitAI && cmpHolderPosition)
						cmpHolderUnitAI.lastShorelinePosition = cmpHolderPosition.GetPosition();
					Engine.PostMessage(this.pickup, MT_PickupCanceled, { "entity": this.entity });
					delete this.pickup;
				}

				if (this.IsTurret())
					this.SetNextState("IDLE");

				return false;
			}
		}
		else
		{
			// Unable to reach the target, try again (or follow if it is a moving target)
			// except if the does not exits anymore or its orders have changed
			if (this.pickup)
			{
				var cmpUnitAI = Engine.QueryInterface(this.pickup, IID_UnitAI);
				if (!cmpUnitAI || !cmpUnitAI.HasPickupOrder(this.entity))
				{
					this.FinishOrder();
					return true;
				}

			}
			if (this.MoveToTarget(target))
			{
				this.SetNextState("APPROACHING");
				return false;
			}
		}
	}
	// Garrisoning failed for some reason, so finish the order
	this.FinishOrder();
	return true;
};

UnitAI.prototype.UnitFsmSpec.INDIVIDUAL.GATHER.GATHERING.Timer = function(msg) {
	var resourceTemplate = this.order.data.template;
	var resourceType = this.order.data.type;

	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	if (!cmpOwnership)
		return;

	var cmpSupply = Engine.QueryInterface(this.gatheringTarget, IID_ResourceSupply);
	if (cmpSupply && cmpSupply.IsAvailable(cmpOwnership.GetOwner(), this.entity))
	{
		// Check we can still reach and gather from the target
		if (this.CheckTargetRange(this.gatheringTarget, IID_ResourceGatherer) && this.CanGather(this.gatheringTarget))
		{
			// Gather the resources:

			var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);

			// Try to gather treasure
			if (cmpResourceGatherer.TryInstantGather(this.gatheringTarget))
				return;

			// Collect from the target
			var status = cmpResourceGatherer.PerformGather(this.gatheringTarget);

			// If we've collected as many resources as possible,
			// return to the nearest dropsite
			if (status.filled)
			{
				var nearby = this.FindNearestDropsite(resourceType.generic);
				if (nearby)
				{
					// (Keep this Gather order on the stack so we'll
					// continue gathering after returning)
					this.PushOrderFront("ReturnResource", { "target": nearby, "force": false });
					return;
				}

				// Oh no, couldn't find any drop sites. Give up on gathering.
				this.FinishOrder();
				return;
			}

			// We can gather more from this target, do so in the next timer
			if (!status.exhausted)
				return;
		}
		else
		{
			// Try to follow the target
			if (this.MoveToTargetRange(this.gatheringTarget, IID_ResourceGatherer))
			{
				this.SetNextState("APPROACHING");
				return;
			}

			// Can't reach the target, or it doesn't exist any more

			// We want to carry on gathering resources in the same area as
			// the old one. So try to get close to the old resource's
			// last known position

			var maxRange = 8; // get close but not too close
			if (this.order.data.lastPos &&
				this.MoveToPointRange(this.order.data.lastPos.x, this.order.data.lastPos.z,
					0, maxRange))
			{
				this.SetNextState("APPROACHING");
				return;
			}
		}
	}

	// We're already in range, can't get anywhere near it or the target is exhausted.

	var herdPos = this.order.data.initPos;

	// Give up on this order and try our next queued order
	// but first check what is our next order and, if needed, insert a returnResource order
	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	if (cmpResourceGatherer.IsCarrying(resourceType.generic) &&
		this.orderQueue.length > 1 && this.orderQueue[1] !== "ReturnResource" &&
		(this.orderQueue[1].type !== "Gather" || this.orderQueue[1].data.type.generic !== resourceType.generic))
	{
		let nearby = this.FindNearestDropsite(resourceType.generic);
		if (nearby)
			this.orderQueue.splice(1, 0, { "type": "ReturnResource", "data": { "target": nearby, "force": false } });
	}
	if (this.FinishOrder())
		return;

	// No remaining orders - pick a useful default behaviour

	// Try to find a new resource of the same specific type near our current position:
	// Also don't switch to a different type of huntable animal
	var nearby = this.FindNearbyResource(function(ent, type, template) {
		return (
			(type.generic == "treasure" && resourceType.generic == "treasure")
			|| (type.specific == resourceType.specific
			&& (type.specific != "meat" || resourceTemplate == template) && type.generic != "wood")
		);
	});
	if (nearby)
	{
		this.PerformGather(nearby, false, false);
		return;
	}

	// If hunting, try to go to the initial herd position to see if we are more lucky
	if (herdPos)
	{
		this.GatherNearPosition(herdPos.x, herdPos.z, resourceType, resourceTemplate);
		return;
	}

	// Nothing else to gather - if we're carrying anything then we should
	// drop it off, and if not then we might as well head to the dropsite
	// anyway because that's a nice enough place to congregate and idle

	var nearby = this.FindNearestDropsite(resourceType.generic);
	if (nearby)
	{
		this.PushOrderFront("ReturnResource", { "target": nearby, "force": false });
		return;
	}

	// No dropsites - just give up
}

UnitAI.prototype.UnitFsm = new FSM(UnitAI.prototype.UnitFsmSpec);


UnitAI.prototype.CanGarrison = function(target)
{
	// Formation controllers should always respond to commands
	// (then the individual units can make up their own minds)
	if (this.IsFormationController())
		return true;

	var cmpGarrisonHolder = Engine.QueryInterface(target, IID_GarrisonHolder);
	if (!cmpGarrisonHolder)
		return false;

	// Verify that the target is owned by this entity's player or a mutual ally of this player
	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	if (!cmpOwnership || !(IsOwnedByPlayer(cmpOwnership.GetOwner(), target) || IsOwnedByMutualAllyOfPlayer(cmpOwnership.GetOwner(), target)))
		return false;

	// Don't let animals garrison for now
	// (If we want to support that, we'll need to change Order.Garrison so it
	// doesn't move the animal into an INVIDIDUAL.* state)
	//if (this.IsAnimal())
		//return false;

	return true;
};

UnitAI.prototype.CheckTargetRange = function(target, iid, type)
{
	var cmpRanged = Engine.QueryInterface(this.entity, iid);
	if (!cmpRanged)
		return false;
	var range = cmpRanged.GetRange(type);

	if (!target) return false;
	
	var cmpUnitMotion = Engine.QueryInterface(this.entity, IID_UnitMotion);
	return cmpUnitMotion.IsInTargetRange(target, range.min, range.max);
};

/**
 * Returns true if the target entity is visible through the FoW/SoD.
 */
UnitAI.prototype.CheckTargetVisible = function(target)
{
	if (!target) return false;
	
	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	if (!cmpOwnership)
		return false;

	var cmpRangeManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager);
	if (!cmpRangeManager)
		return false;

	// Entities that are hidden and miraged are considered visible
	var cmpFogging = Engine.QueryInterface(target, IID_Fogging);
	if (cmpFogging && cmpFogging.IsMiraged(cmpOwnership.GetOwner()))
		return true;

	if (cmpRangeManager.GetLosVisibility(target, cmpOwnership.GetOwner()) == "hidden")
		return false;

	// Either visible directly, or visible in fog
	return true;
};

UnitAI.prototype.CanHeal = function(target)
{
	// Formation controllers should always respond to commands
	// (then the individual units can make up their own minds)
	if (this.IsFormationController())
		return true;

	// Verify that we're able to respond to Heal commands
	var cmpHeal = Engine.QueryInterface(this.entity, IID_Heal);
	if (!cmpHeal)
		return false;

	// Verify that the target is alive
	if (!this.TargetIsAlive(target))
		return false;

	// Verify that the target is not unhealable (or at max health)
	var cmpHealth = Engine.QueryInterface(target, IID_Health);
	if (!cmpHealth || cmpHealth.IsUnhealable())
		return false;

	// Verify that the target has no unhealable class
	var cmpIdentity = Engine.QueryInterface(target, IID_Identity);
	if (!cmpIdentity)
		return false;

	if (MatchesClassList(cmpIdentity.GetClassesList(), cmpHeal.GetUnhealableClasses()))
		return false;

	// Verify that the target is a healable class
	if (MatchesClassList(cmpIdentity.GetClassesList(), cmpHeal.GetHealableClasses()))
		return true;

	return false;
};

UnitAI.prototype.CanReturnResource = function(target, checkCarriedResource)
{
	if (this.IsTurret())
		return false;
	// Formation controllers should always respond to commands
	// (then the individual units can make up their own minds)
	if (this.IsFormationController())
		return true;

	// Verify that we're able to respond to ReturnResource commands
	var cmpResourceGatherer = Engine.QueryInterface(this.entity, IID_ResourceGatherer);
	if (!cmpResourceGatherer)
		return false;

	// Verify that the target is a dropsite
	var cmpResourceDropsite = Engine.QueryInterface(target, IID_ResourceDropsite);
	if (!cmpResourceDropsite)
		return false;

	if (checkCarriedResource)
	{
		let AtLeastOne = false;
		for (let resource in cmpResourceGatherer.carrying) {
			if (cmpResourceDropsite.AcceptsType(resource)) {
				AtLeastOne = true;
				break;
			}
		}
		
		if (!AtLeastOne) {
			return false;
		}
	}

	// Verify that the dropsite is owned by this entity's player (or a mutual ally's if allowed)
	var cmpOwnership = Engine.QueryInterface(this.entity, IID_Ownership);
	if (cmpOwnership && IsOwnedByPlayer(cmpOwnership.GetOwner(), target))
		return true;
	var cmpPlayer = QueryOwnerInterface(this.entity);
	return cmpPlayer && cmpPlayer.HasSharedDropsites() && cmpResourceDropsite.IsShared() &&
	       cmpOwnership && IsOwnedByMutualAllyOfPlayer(cmpOwnership.GetOwner(), target);
};

UnitAI.prototype.IsDomestic = function()
{
	return true;
};

UnitAI.prototype.CanGuard = function()
{
	// Formation controllers should always respond to commands
	// (then the individual units can make up their own minds)
	if (this.IsFormationController())
		return true;

	return this.template.CanGuard == "true";
};

UnitAI.prototype.AddGuard = function(target)
{
	if (!this.CanGuard())
		return false;
	
	if (target == this.entity) return false;

	var cmpGuard = Engine.QueryInterface(target, IID_Guard);
	if (!cmpGuard)
		return false;

	this.isGuardOf = target;
	this.guardRange = 2;
	cmpGuard.AddGuard(this.entity);
	return true;
};
