let House = {};

House.Schema = "<a:help>Deals with spawning citizens</a:help>" +
	"<a:example>" +
		"<Slots>1</Slots>" +
	"</a:example>" +
	
	"<element name='Slots' a:help='The number of citizens that can live in this house.'>" +
		"<data type='nonNegativeInteger'/>" +
	"</element>" +
	
	"<element name='Male' a:help='The male template name'>" +
		"<text/>" +
	"</element>" +
	
	"<element name='Female' a:help='The female template name'>" +
		"<text/>" +
	"</element>";

House.Init = function() {
	this.slots = [];
	this.odd = false;
	this.vacancy = true;
	
	for (let i = 0; i < +this.template.Slots; i++) {
		this.slots.push(null);
	}
		
}

//Ungarrrison unit.
House.Kick = function(ent) {
	this.Get(IID_GarrisonHolder).Eject(ent, false);
}

//Move the unit inside?
House.OnGarrisonedUnitsChanged = function (msg) {
	if (msg.added.length > 0) {
		for (let entity of msg.added) {
			
			let Citizen = rts.Call(IID_Citizen, entity);
			
			//Check if there is room!
			if (this.vacancy && Citizen) {

				let done = false;
				let still_has_vacancy = false;
				for (let i = 0; i < this.slots.length; i++) {
					if (!done && this.slots[i] == null) {
						this.slots[i] = entity;
						done = true;
					}
					if (done && this.slots[i] == null) {
						still_has_vacancy = true;
					}
				}
				this.vacancy = still_has_vacancy;
				
				//Kick them from their last house.
				let OldHouse = rts.Call(IID_House, Citizen.home);
				OldHouse.Evict(entity);
				
				Citizen.home = this.entity;

			}
			
			if (this.Get(IID_Company) || this.Get(IID_Training)) {
				continue;
			}
			this.NewTimer().Run("Kick", entity).After(1000).Start();
		}
	}
}

//Fill slot 'i' with a new citizen.
House.FillSlot = function(i) {
	let citizen;
	
	if (this.odd)
		if ( (i % 2 == 0) )
			citizen = this.Spawn(this.template.Female)
		else
			citizen = this.Spawn(this.template.Male);
	else 
		if ( (i % 2 == 0) )
			citizen = this.Spawn(this.template.Male)
		else
			citizen = this.Spawn(this.template.Female);
	
	if (rts.Call(IID_Citizen, citizen)) {
		rts.Call(IID_Citizen, citizen).home = this.entity;
	}
	
	return citizen;
}

House.OnCreate = function() {
	if (+this.template.Slots % 2 != 0) {
		this.odd = randBool();
	}
	this.vacancy = true;
}

//Evict 'ent' from the house.
House.Evict = function(ent) {
	for (let i = 0; i < +this.slots.length; i++) {
		if (this.slots[i] == ent) {
			this.slots[i] = null;
			this.vacancy = true;
			return;
		}
	}
}

//Replace 'ent' in this house with 'new_ent'.
House.Rename = function(ent, new_ent) {
	for (let i = 0; i < +this.slots.length; i++) {
		if (this.slots[i] == ent) {
			this.slots[i] = new_ent;
			return;
		}
	}
}

//Fill the house, so it is full.
House.Fill = function() {
	if (this.slots.length < +this.template.Slots) {
		for (let i = 0; i < +this.template.Slots-this.slots.length; i++) {
			this.slots.push(null);
		}
	}
	
	this.vacancy = false;
	
	//If this is a new house, create the slots!
	if (this.slots == null) {
		this.slots = [];
		for (let i = 0; i < +this.template.Slots; i++) {
			this.slots.push(this.FillSlot(i));
		}
		
		return;
	}
	
	for (let i = 0; i < this.slots.length; i++) {
		if (this.slots[i] == null) {
			this.slots[i] = this.FillSlot(i);
			continue;
		}
	}
}

//User Interface.
House.Commands = {};
House.Commands.Fill = "Fill";

House.Capacity = function() {
	return this.slots.length;
}

House.Information = function() {
	let icons = [];
	let names = [];
	
	if (this.slots) {
		for (let i = 0; i < this.slots.length; i++) {
			if (this.slots[i] != null) {
				let Identity = rts.Call(IID_Identity, this.slots[i]);
				if (Identity) {
					icons.push(Identity.template.Icon);
					let citizen = rts.Call(IID_Citizen, this.slots[i]);
					if (citizen) {
						names.push(citizen.name)
					} else {
						names.push(Identity.template.SpecificName);
					}
				} else {
					icons.push(null);
					names.push(null);
				}
			} else {
				icons.push(null);
				names.push(null);
			}
		}
	}
	
	return {
		Slots: this.slots,
		Vacancy: this.vacancy,
		Odd: this.odd,
		Icons: icons,
		Names: names,
	};
}

rts.RegisterComponent(IID_House, "House", House);
