/**
 * MODIFIED FOR BLOCKADE MOD!
 * Transfer our carried resources to our owner immediately.
 * Only resources of the given types will be transferred.
 * (This should typically be called after reaching a dropsite).
 */
ResourceGatherer.prototype.CommitResources = function(dropsite)
{
	let node = rts.Call(IID_ResourceNode, dropsite.entity);
	if (node) {
		
		//Add the type they are carrying to the resource node.
		for (let type in this.carrying) {
			if (type == "food") continue;
			
			if (type.replace("credit.","") != type) {
				delete this.carrying[type];
				type = type.replace("credit.", "")
				
				if (!node.Has(type)) continue; //Protest?
				
				node.Decrease(type)
				
				this.carrying[type] = 1;
				continue;
			}

			node.Increase(type);
			delete this.carrying[type];
		}
		
		return;
	}
	
  	/*let types = dropsite.GetTypes();
  
	let cmpPlayer = QueryOwnerInterface(this.entity);
	
	
	
	let lastType = "";

	let NeedToWait = false; 
	
	let ThereAreCredits = false;
	
	//Credit magic
	for (let type in this.carrying) {
		if (type == "food") continue;
		
		if (type.replace("credit.","") != type) {
			
			let credit = type.replace("credit.", "")
			
			delete this.carrying[type];

			if (!(credit in dropsite.stored) || dropsite.stored[credit] == 0) {
				NeedToWait = true;
				//TODO protest?
				continue;
			}
			
			if (this.carrying[credit] != null) {
				dropsite.GiveResource({type: type, amount: this.carrying[type]});
			}

			if (dropsite.stored[credit] > 10) {
				this.carrying[credit] = 10;
				dropsite.stored[credit] -= 10;
			} else {
				this.carrying[credit] = dropsite.stored[credit];
				dropsite.stored[credit] = 0;
			}

			delete this.carrying[type];
			
			ThereAreCredits = true
			break;
		}
	}	
	
	if (!ThereAreCredits) {

		for (let type of types)
			if (type in this.carrying && type != "food")
			{
				if (dropsite.AcceptsType(type)) {
					if (this.carrying[type] != null) {
						dropsite.GiveResource({type: type, amount: this.carrying[type]});
					}
					
					if (Resources.GetResource(type).food) {
						this.carrying.food -= this.carrying[resource];
					}
					
					//cmpPlayer.AddResource(type, this.carrying[type]);
					delete this.carrying[type];
				}
		}
		
		if (types.indexOf("food") != -1) {
			for (let resource in this.carrying) {
				if (resource == "food") continue;

				if (Resources.GetResource(resource).food) {
					dropsite.GiveResource({type: resource, amount: this.carrying[resource]});
					
					this.carrying.food -= this.carrying[resource];
					delete this.carrying[resource];
				}
			}
		}
	
	}*/
	
	if (this.HasFood()) {
		this.lastCarriedType = {generic: "food", specific: ""};
	}
	if ("wood" in this.carrying) {
		this.lastCarriedType = {generic: "wood", specific: ""};
	}
	if ("stone" in this.carrying) {
		this.lastCarriedType = {generic: "stone", specific: ""};
	}
	if ("wood" in this.carrying && "stone" in this.carrying) {
		this.lastCarriedType = {generic: "metal", specific: ""};
	}

	Engine.PostMessage(this.entity, MT_ResourceCarryingChanged, { "to": this.GetCarryingStatus() });
}; 

ResourceGatherer.prototype.HasFood = function() {
	return this.carrying.food > 0
}

ResourceGatherer.prototype.EatFood = function() {
	
	for (let resource in this.carrying) {
		if (resource == "food") continue;
		
		let time = Resources.GetResource(resource).food;
		if (time && this.carrying[resource] > 0) {
			this.carrying[resource] -= 1;
			this.carrying.food -= 1;
			
			let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
			if (StatusBars && StatusBars.enabled)
				StatusBars.RegenerateSprites();
			
			return time*1000;
		}
	}
	
	error("Cannot eat non-existing food")
	return 3
}


ResourceGatherer.prototype.Init = function()
{
	this.carrying = {}; // { generic type: integer amount currently carried }
	// (Note that this component supports carrying multiple types of resources,
	// each with an independent capacity, but the rest of the game currently
	// ensures and assumes we'll only be carrying one type at once)

	// The last exact type gathered, so we can render appropriate props
	this.lastCarriedType = undefined; // { generic, specific }

	this.RecalculateGatherRatesAndCapacities();
	
	this.deficiency = 0;
	this.experience = {};
	
	this.carrying.food = 0;
};

/**
 * Gather from the target entity. This should only be called after a successful range check,
 * and if the target has a compatible ResourceSupply.
 * Call interval will be determined by gather rate, so always gather 1 amount when called.
 */
ResourceGatherer.prototype.PerformGather = function(target)
{
	if (!this.GetTargetGatherRate(target))
		return { "exhausted": true };

	let gatherAmount = 1;

	let cmpResourceSupply = Engine.QueryInterface(target, IID_ResourceSupply);
	let type = cmpResourceSupply.GetType();
	
	//HACK for wood
	if (type.generic == "wood") {
		gatherAmount += this.deficiency;
	}

	// Initialise the carried count if necessary
	if (!this.carrying[type.generic])
		this.carrying[type.generic] = 0;
	
	if (this.carrying[type.generic] >= this.GetCapacity(type.generic)) {
		return {
			"amount": 0,
			"exhausted": false,
			"filled": true
		};
	}

	// Find the maximum so we won't exceed our capacity
	//let maxGathered = this.GetCapacity(type.generic) - this.carrying[type.generic];

	let status = cmpResourceSupply.TakeResources(gatherAmount);
	
	//HACK for wood
	if (type.generic == "wood") {
		//Put wasted resources in the pot.
		let Pot = rts.Get(IID_ResourcePot);
		Pot.Add(type.generic, this.deficiency);
		
		//TODO check seed carrying capacity.
		if (status.exhausted && Pot.Has("wood", 200)) {
			Pot.Take("wood", 100);
			if (! ("seeds" in this.carrying)) this.carrying.seeds = 0;
			this.carrying["seeds"] += 1;
		}
	}

	//Lucky gatherers will recieve metal when they mine stone.
	if (type.generic == "stone") {
		let Pot = rts.Get(IID_ResourcePot);
		Pot.CountingStone = false;
		
		
		if (status.amount > 0 && Pot.Has("metal", 1)) {
			if (randFloat(0, 1) < this.luck) {
				Pot.Take("metal", 1);
				
				// Initialise the carried count if necessary
				if (!this.carrying["metal"])
					this.carrying["metal"] = 0;
				
				this.carrying["metal"] += 1;
				
			}
		}
	}
	
	if (status.amount > 0) {
		this.carrying[type.generic] += 1;
		if (!(type.generic in this.experience)) {
			this.experience[type.generic] = 0;
		}
		
		this.experience[type.generic] += status.amount;
		
		if (type.generic == "wood" && this.experience["wood"] >= 100) {
			this.experience["wood"] = 0;
			this.deficiency++;
			if (this.deficiency >= 99) {
				this.deficiency = 99;
			}
		}
	}

	this.lastCarriedType = type;
	
	if (this.needs && this.needs == type.generic) {
		this.needs = null;
		
		let StatusBars = Engine.QueryInterface(this.entity, IID_StatusBars);
		if (StatusBars && StatusBars.enabled)
			StatusBars.RegenerateSprites();
	}

	// Update stats of how much the player collected.
	// (We have to do it here rather than at the dropsite, because we
	// need to know what subtype it was)
	let cmpStatisticsTracker = QueryOwnerInterface(this.entity, IID_StatisticsTracker);
	if (cmpStatisticsTracker)
		cmpStatisticsTracker.IncreaseResourceGatheredCounter(type.generic, status.amount, type.specific);

	Engine.PostMessage(this.entity, MT_ResourceCarryingChanged, { "to": this.GetCarryingStatus() });

	let Unit = Engine.QueryInterface(this.entity, IID_UnitAI);
	if (!this.protesting && status.amount == 0) {
		
		//FIRST try to find a nearby resource we can gather from!
		let LOS = Engine.QueryInterface(this.entity, IID_Vision).GetRange();
		
		let Supplies = rts.Find(IID_ResourceSupply).Near(this).Radius(LOS).List();
		for (let supply of Supplies) {
			if (!supply.recharging && supply.GetType().generic == type.generic) {
				Unit.FinishOrder();
				Unit.PushOrderFront("Gather", {target: supply.entity, type: type});
				this.protesting = false;
				return {
					"amount": 0,
					"exhausted": status.exhausted,
					"filled": this.carrying[type.generic] >= this.GetCapacity(type.generic)
				};
			}
		}


		this.protesting = true;
		Unit.SetAnimationVariant("combat");
		Unit.SelectAnimation("attack_capture");

	} else if (this.protesting && status.amount > 0) {
		
		Unit.SetDefaultAnimationVariant();
		var typename = Resources.GetResource(Unit.order.data.type.generic).gatherAnimation;
		if (!typename) {
			typename ="gather_" + Unit.order.data.type.specific;
		}
		Unit.SelectAnimation(typename);
		
	}
	
	
	if (Resources.GetResource(type.generic).food) {
		this.carrying.food += status.amount;
	}
	
	return {
		"amount": status.amount,
		"exhausted": status.exhausted,
		"filled": this.carrying[type.generic] >= this.GetCapacity(type.generic)
	};
};
