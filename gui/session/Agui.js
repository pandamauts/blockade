let gui = {};

gui.UnitAction = 1;


gui.UnitActions = [];
gui.DynamicUnitActions = [];

gui.Following = 0;
gui.FPS = false;

gui.RegisterButton = function(type, definition) {
	if (type == gui.UnitAction) {
		gui.UnitActions.push(definition);
	}
};

gui.RegisterDynamicButton = function(type, definition) {
	if (type == gui.UnitAction) {
		gui.DynamicUnitActions.push(definition);
	}
};

gui.Select = function(ent) {
	g_Selection.reset();
	g_Selection.addList([ent]);
}

gui.Selected = function() {
	return g_Selection.toList()[0];
}

gui.StateOf = function(ent) {
	return GetEntityState(ent);
}

gui.Goto = function(ent) {
	let entState = GetEntityState(ent);
	if (!entState || !entState.position)
		return;

	let position = entState.position;
	Engine.CameraMoveTo(position.x, position.z);
}

gui.Follow = function(ent) {
	if (ent == gui.Following) {
		Engine.CameraFollowFPS(ent);
		gui.Following = null;
		
		
		gui.FPS = true;
		print("CONFIG: ", Engine.ConfigDB_GetValue("user", "hotkey.camera.left"), "\n");
		
		return;
	}
	gui.FPS = false;
	setCameraFollow(ent);
	gui.Following = ent;
}

gui.Plan = function(template) {
	startBuildingPlacement(template, gui._data.playerState);
}

gui.SendCommandToSelectedEntities = function(cmd, data) {
	let list = {
		"type": cmd,
		"entities": g_Selection.toList(),
		"queued": false,
	};
	if (data) {
		for (let i in data) {
			list[i] = data[i];
		}
	}
	Engine.PostNetworkCommand(list);
}

gui.Object = function(object) {
	this.object = object;
	
	this.left = 0;
	this.right = 0;
	this.bottom = 0;
	this.top = 0;
	
	this.rleft = 0;
	this.rright = 0;
	this.rbottom = 0;
	this.rtop = 0;
}

gui.Object.prototype.AttachRight = function() {
	this.rleft = 100;
	this.rright = 100;
}

gui.Object.prototype.SetStyle = function(style) {
	this.style = style;
}

gui.Object.prototype.SetSize = function(width, height) {
	this.width = width;
	this.height = height;
}

gui.Object.prototype.SetOffset = function(x, y) {
	this.left = x;
	this.bottom = y;
}

gui.Object.prototype.SetText = function(msg) {
	this.caption = msg;
}

gui.Object.prototype.SetBackgroundImage = function(img) {
	this.sprite = img;
}

gui.Object.prototype.Update = function() {
	
	if (this.width) {
		this.right = this.left+this.width;
	}
	
	if (this.height) {
		this.top = this.bottom+this.height;
	}
	
	if (this.sprite) {
		this.object.sprite = this.sprite;
	}
	
	if (this.caption) {
		this.object.caption = this.caption;
	}
	
	if (this.style) {
		this.object.style = this.style;
	}
	
	let size = this.rleft+"%";
	if (this.left < 0) {
		size += "-";
	} else {
		size += "+";
	}
	size += Math.abs(this.left)+" ";
	
	size += this.rbottom+"%";
	if (this.bottom < 0) {
		size += "-";
	} else {
		size += "+";
	}
	size += Math.abs(this.bottom)+" ";
	
	size += this.rright+"%";
	if (this.right < 0) {
		size += "-";
	} else {
		size += "+";
	}
	size += Math.abs(this.right)+" ";
	
	size += this.rtop+"%";
	if (this.top < 0) {
		size += "-";
	} else {
		size += "+";
	}
	size += Math.abs(this.top);

	print(size, "\n");
	this.object.size = size;
}

gui.Get = function(name) {
	let object = new gui.Object(Engine.GetGUIObjectByName(name));
	return object
}
