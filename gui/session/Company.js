
gui.RegisterDynamicButton(gui.UnitAction, function(ent) {	
	if (ent.Company && ent.Company.HasWorkers) {
		return {
			Tooltip: "Eject Workers",
			Classes: ["selected", "unit", "Company"],

			Requires: "Company",
			
			Icon: "stretched:session/icons/pack.png",

			Action: function() {
				gui.SendCommandToSelectedEntities("EjectAll");
			}
		}
	}
})
