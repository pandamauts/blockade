
gui.RegisterDynamicButton(gui.UnitAction, function(ent) {
	if (ent.Builds && ent.Builds.List) {
		return {
			Tooltip: "Back",
			Classes: ["selected", "unit", "Builds"],

			Requires: "Builds",

			Icon: "stretched:session/icons/back.png",

			Action: function() {
				gui.SendCommandToSelectedEntities("SelectBuildList", {Index: -1});
			}
		}
	}
})

for (let i=0; i < 7; i++) {
	let Names = ["General", "Food", "Trees", "Economy", "Military", "Defense", "Props"];
	let Icons = [
		"stretched:session/icons/actions/house.png",
		"stretched:session/icons/actions/food.png",
		"stretched:session/icons/actions/trees.png",
		"stretched:session/icons/actions/economy.png",
		"stretched:session/icons/actions/millitary.png",
		"stretched:session/icons/actions/defense.png",
		"stretched:session/icons/actions/props.png",
	];
	let j = i;
	gui.RegisterDynamicButton(gui.UnitAction, function(ent) {

		if (ent.Builds && !ent.Builds.List) {
			return {
				Tooltip: Names[j],
				Classes: ["selected", "unit", "Builds"],

				Requires: "Builds",

				Icon: Icons[j],

				Action: function() {
					gui.SendCommandToSelectedEntities("SelectBuildList", {Index: j});
				}
			};
		}

		if (ent.Builds && ent.Builds.List && ent.Builds.List.length > j) {

			let icon = "stretched:session/portraits/units/global_slave.png";

			let template = GetTemplateData(ent.Builds.List[j]);



			return {
				Tooltip: template.name.generic+"\n"+getEntityCostTooltip(template),
				Classes: ["selected", "unit", "Builds"],

				Requires: "Builds",

				Icon: "stretched:session/portraits/"+template.icon,

				Action: function() {
					gui.Plan(ent.Builds.List[j]);
				}
			}
		}
	})
}
