for (let i=0; i < 3; i++) {
	let j = i;
	gui.RegisterDynamicButton(gui.UnitAction, function(ent) {
		
		if (ent.Upgrades && ent.Upgrades.Into && ent.Upgrades.Into.length > j) {
			
			let template = GetTemplateData(ent.Upgrades.Into[j]);
			
			return {
				Tooltip: "Upgrade",
				Classes: ["selected", "unit", "Upgrades"],

				Requires: "Upgrades",
				
				Icon: "stretched:session/icons/upgrades/"+template.nativeCiv+".png",

				Action: function() {
					gui.SendCommandToSelectedEntities("Upgrade", {Into: j});
				}
			}
		}
	})
}
