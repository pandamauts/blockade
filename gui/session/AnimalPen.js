for (let i=0; i < 6; i++) {
	let j = i;
	gui.RegisterDynamicButton(gui.UnitAction, function(ent) {
		
		if (ent.AnimalPen && ent.AnimalPen.Slots && ent.AnimalPen.Slots.length > j && ent.AnimalPen.Slots[j]) {
			
			let icon = "stretched:session/portraits/units/global_slave.png";
			
			if (icon != "") {
				icon = "stretched:session/portraits/"+ent.AnimalPen.Icon;
			}
			
			return {
				Tooltip: "Animal",
				Classes: ["selected", "unit", "AnimalPen"],

				Requires: "AnimalPen",
				
				Icon: icon,

				Action: function() {
					gui.Select(ent.AnimalPen.Slots[j]);
					gui.Follow(ent.AnimalPen.Slots[j]);
				}
			}
		}
	})
}

gui.RegisterDynamicButton(gui.UnitAction, function(ent) {	
	if (ent.AnimalPen && ent.AnimalPen.ReadyToBreed) {
		return {
			Tooltip: "Breed",
			Classes: ["selected", "unit", "AnimalPen"],

			Requires: "AnimalPen",
			
			Icon: "stretched:session/icons/pack.png",

			Action: function() {
				gui.SendCommandToSelectedEntities("Fill");
			}
		}
	}
})
