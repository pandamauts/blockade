gui.RegisterButton(gui.UnitAction, {
	Tooltip: "Carry Food",
	Classes: ["selected", "unit", "Trader"],

	Requires: "trader",
	Icon: "stretched:session/icons/resources/food.png",
	
	Action: function() {
		gui.SendCommandToSelectedEntities("ChangeTrade", {resource: "food"});
	}
})

gui.RegisterButton(gui.UnitAction, {
	Tooltip: "Carry Wood",
	Classes: ["selected", "unit", "Trader"],

	Requires: "trader",
	Icon: "stretched:session/icons/resources/wood.png",
	
	Action: function() {
		gui.SendCommandToSelectedEntities("ChangeTrade", {resource: "wood"});
	}
})


gui.RegisterButton(gui.UnitAction, {
	Tooltip: "Carry Metal",
	Classes: ["selected", "unit", "Trader"],

	Requires: "trader",
	Icon: "stretched:session/icons/resources/metal.png",
	
	Action: function() {
		gui.SendCommandToSelectedEntities("ChangeTrade", {resource: "metal"});
	}
})


gui.RegisterButton(gui.UnitAction, {
	Tooltip: "Carry Stone",
	Classes: ["selected", "unit", "Trader"],

	Requires: "trader",
	Icon: "stretched:session/icons/resources/stone.png",
	
	Action: function() {
		gui.SendCommandToSelectedEntities("ChangeTrade", {resource: "stone"});
	}
})
