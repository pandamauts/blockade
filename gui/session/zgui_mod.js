g_SelectionPanels.Mod = {
	"getMaxNumberOfItems": function()
	{
		return 24 - getNumberOfRightPanelButtons();
	},
	"getItems": function(unitEntStates)
	{
		let items = [];
		
		for (let i = 0; i < gui.UnitActions.length; i++) {
			let button = gui.UnitActions[i];

			if (button.Requires in unitEntStates[0]) {
				items.push(button);
			}

		}
		
		for (let i = 0; i < gui.DynamicUnitActions.length; i++) {
			let button = gui.DynamicUnitActions[i](unitEntStates[0]);

			if (button) {
				items.push(button);
			}
		}
		
		return items;
	},
	"setupButton": function(data)
	{
		
		data.button.onPress = function() {
			gui._data = data;
			data.item.Action();
		}

		data.button.tooltip = data.item.Tooltip;

		data.icon.sprite = data.item.Icon || "stretched:session/icons/pack.png";

		data.button.enabled = controlsPlayer(data.player);

		setPanelObjectPosition(data.button, data.i + getNumberOfRightPanelButtons(), data.rowLength);
		return true;
	}
}; 

let g_PanelsOrder = [
	"Mod", //User buttons
]; 


g_unitPanelButtons["Mod"] = 0;
