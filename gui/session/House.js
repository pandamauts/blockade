for (let i=0; i < 6; i++) {
	let j = i;
	gui.RegisterDynamicButton(gui.UnitAction, function(ent) {
		
		if (ent.House && ent.House.Slots && ent.House.Slots.length > j && ent.House.Slots[j]) {
			
			let icon = "stretched:session/portraits/"+ent.House.Icons[j];
			
			return {
				Tooltip: ent.House.Names[j],
				Classes: ["selected", "unit", "House"],

				Requires: "House",
				
				Icon: icon,

				Action: function() {
					gui.Goto(ent.House.Slots[j]);
					gui.Select(ent.House.Slots[j]);
					gui.Follow(ent.House.Slots[j]);
				}
			}
		}
	})
}

gui.RegisterDynamicButton(gui.UnitAction, function(ent) {	
	if (ent.House && ent.House.Vacancy) {
		return {
			Tooltip: "Fill House",
			Classes: ["selected", "unit", "House"],

			Requires: "House",
			
			Icon: "stretched:session/icons/pack.png",

			Action: function() {
				gui.SendCommandToSelectedEntities("Fill");
			}
		}
	}
})
