function layoutSelectionSingle()
{
	Engine.GetGUIObjectByName("detailsAreaSingle").hidden = false;
	Engine.GetGUIObjectByName("detailsAreaMultiple").hidden = true;
}

function layoutSelectionMultiple()
{
	Engine.GetGUIObjectByName("detailsAreaMultiple").hidden = false;
	Engine.GetGUIObjectByName("detailsAreaSingle").hidden = true;
}

function getResourceTypeDisplayName(resourceType)
{
	return resourceNameFirstWord(
		resourceType.generic == "treasure" ?
			resourceType.specific :
			resourceType.generic);
}

// Updates the health bar of garrisoned units
function updateGarrisonHealthBar(entState, selection)
{
	if (!entState.garrisonHolder)
		return;

	// Summing up the Health of every single unit
	let totalGarrisonHealth = 0;
	let maxGarrisonHealth = 0;
	for (let selEnt of selection)
	{
		let selEntState = GetEntityState(selEnt);
		if (selEntState.garrisonHolder)
			for (let ent of selEntState.garrisonHolder.entities)
			{
				let state = GetEntityState(ent);
				totalGarrisonHealth += state.hitpoints || 0;
				maxGarrisonHealth += state.maxHitpoints || 0;
			}
	}

	// Configuring the health bar
	let healthGarrison = Engine.GetGUIObjectByName("healthGarrison");
	healthGarrison.hidden = totalGarrisonHealth <= 0;
	if (totalGarrisonHealth > 0)
	{
		let healthBarGarrison = Engine.GetGUIObjectByName("healthBarGarrison");
		let healthSize = healthBarGarrison.size;
		healthSize.rtop = 100 - 100 * Math.max(0, Math.min(1, totalGarrisonHealth / maxGarrisonHealth));
		healthBarGarrison.size = healthSize;

		healthGarrison.tooltip = getCurrentHealthTooltip({
			"hitpoints": totalGarrisonHealth,
			"maxHitpoints": maxGarrisonHealth
		});
	}
}

String.prototype.plural = function(revert){

	var plural = {
		'(quiz)$'               : "$1zes",
		'^(ox)$'                : "$1en",
		'([m|l])ouse$'          : "$1ice",
		'(matr|vert|ind)ix|ex$' : "$1ices",
		'(x|ch|ss|sh)$'         : "$1es",
		'([^aeiouy]|qu)y$'      : "$1ies",
		'(hive)$'               : "$1s",
		'(?:([^f])fe|([lr])f)$' : "$1$2ves",
		'(shea|lea|loa|thie)f$' : "$1ves",
		'sis$'                  : "ses",
		'([ti])um$'             : "$1a",
		'(tomat|potat|ech|her|vet)o$': "$1oes",
		'(bu)s$'                : "$1ses",
		'(alias)$'              : "$1es",
		'(octop)us$'            : "$1i",
		'(ax|test)is$'          : "$1es",
		'(us)$'                 : "$1es",
		'([^s]+)$'              : "$1s"
	};

	var singular = {
		'(quiz)zes$'             : "$1",
		'(matr)ices$'            : "$1ix",
		'(vert|ind)ices$'        : "$1ex",
		'^(ox)en$'               : "$1",
		'(alias)es$'             : "$1",
		'(octop|vir)i$'          : "$1us",
		'(cris|ax|test)es$'      : "$1is",
		'(shoe)s$'               : "$1",
		'(o)es$'                 : "$1",
		'(bus)es$'               : "$1",
		'([m|l])ice$'            : "$1ouse",
		'(x|ch|ss|sh)es$'        : "$1",
		'(m)ovies$'              : "$1ovie",
		'(s)eries$'              : "$1eries",
		'([^aeiouy]|qu)ies$'     : "$1y",
		'([lr])ves$'             : "$1f",
		'(tive)s$'               : "$1",
		'(hive)s$'               : "$1",
		'(li|wi|kni)ves$'        : "$1fe",
		'(shea|loa|lea|thie)ves$': "$1f",
		'(^analy)ses$'           : "$1sis",
		'((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$': "$1$2sis",        
		'([ti])a$'               : "$1um",
		'(n)ews$'                : "$1ews",
		'(h|bl)ouses$'           : "$1ouse",
		'(corpse)s$'             : "$1",
		'(us)es$'                : "$1",
		's$'                     : ""
	};

	var irregular = {
		'move'   : 'moves',
		'foot'   : 'feet',
		'goose'  : 'geese',
		'sex'    : 'sexes',
		'child'  : 'children',
		'man'    : 'men',
		'tooth'  : 'teeth',
		'person' : 'people'
	};

	var uncountable = [
		'sheep', 
		'fish',
		'deer',
		'moose',
		'series',
		'species',
		'money',
		'rice',
		'information',
		'equipment'
	];

	// save some time in the case that singular and plural are the same
	if(uncountable.indexOf(this.toLowerCase()) >= 0)
	return this;

	// check for irregular forms
	for(let word in irregular){

	if(revert){
			var pattern = new RegExp(irregular[word]+'$', 'i');
			var replace = word;
	} else{ var pattern = new RegExp(word+'$', 'i');
			var replace = irregular[word];
	}
	if(pattern.test(this))
		return this.replace(pattern, replace);
	}

	if(revert) var array = singular;
		else  var array = plural;

	// check for matches using regular expressions
	for(let reg in array){

	var pattern = new RegExp(reg, 'i');

	if(pattern.test(this))
		return this.replace(pattern, array[reg]);
	}

	return this;
}

// Fills out information that most entities have
function displaySingle(entState)
{
	// Get general unit and player data
	let template = GetTemplateData(entState.template);
	
	let specificName = template.name.specific;
	
	if (!entState.Temple && !entState.House && !entState.Emotional && !entState.resourceCarrying) {
		Engine.GetGUIObjectByName("selectionDetails").hidden = true;
		return;
	}
	Engine.GetGUIObjectByName("selectionDetails").hidden = false;
	
	/*if (template.tooltip) {
		Engine.GetGUIObjectByName("Tooltip").caption = template.tooltip;
	} else {
		Engine.GetGUIObjectByName("Tooltip").caption = "";
	}*/
	
	Engine.GetGUIObjectByName("House").hidden = true;
	if (entState.Citizen) {
		specificName = entState.Citizen.Name;
		
		if (entState.Citizen.Home) {
			Engine.GetGUIObjectByName("House").hidden = false;
		}
		
		//Engine.GetGUIObjectByName("Job").caption = "I am a "+entState.Citizen.Job;
	}
	
	if (entState.CityState) {
		specificName = entState.CityState.Name;
	}
	
	//Resource [0] is always food.
	Engine.GetGUIObjectByName("inventory1").hidden = true;
	Engine.GetGUIObjectByName("inventory2_1").hidden = true;
	Engine.GetGUIObjectByName("inventory2_2").hidden = true;
	
	Engine.GetGUIObjectByName("inventory3_1").hidden = true;
	Engine.GetGUIObjectByName("inventory3_2").hidden = true;
	Engine.GetGUIObjectByName("inventory3_3").hidden = true;
	
	Engine.GetGUIObjectByName("inventory4_1").hidden = true;
	Engine.GetGUIObjectByName("inventory4_2").hidden = true;
	Engine.GetGUIObjectByName("inventory4_3").hidden = true;
	Engine.GetGUIObjectByName("inventory4_4").hidden = true;
	Engine.GetGUIObjectByName("inventory4_5").hidden = true;
	Engine.GetGUIObjectByName("inventory4_6").hidden = true;
	
	Engine.GetGUIObjectByName("inventory18_1").hidden = true;
	Engine.GetGUIObjectByName("inventory18_2").hidden = true;
	Engine.GetGUIObjectByName("inventory18_3").hidden = true;
	Engine.GetGUIObjectByName("inventory18_4").hidden = true;
	Engine.GetGUIObjectByName("inventory18_5").hidden = true;
	Engine.GetGUIObjectByName("inventory18_6").hidden = true;
	
	if (entState.resourceCarrying) {
		if (entState.resourceCarrying.length == 2) {
			let inventory = Engine.GetGUIObjectByName("inventory1");
			let resource = entState.resourceCarrying[1];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
		}
		
		if (entState.resourceCarrying.length == 3) {
			let inventory = Engine.GetGUIObjectByName("inventory2_1");
			let resource = entState.resourceCarrying[1];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory2_2");
			resource = entState.resourceCarrying[2];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
		}
		
		if (entState.resourceCarrying.length == 4) {
			let inventory = Engine.GetGUIObjectByName("inventory3_1");
			let resource = entState.resourceCarrying[1];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory3_2");
			resource = entState.resourceCarrying[2];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory3_3");
			resource = entState.resourceCarrying[3];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
		}
		
		if (entState.resourceCarrying.length >= 5 && entState.resourceCarrying.length <= 7) {
			let inventory = Engine.GetGUIObjectByName("inventory4_1");
			let resource = entState.resourceCarrying[1];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory4_2");
			resource = entState.resourceCarrying[2];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory4_3");
			resource = entState.resourceCarrying[3];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory4_4");
			resource = entState.resourceCarrying[4];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			if (entState.resourceCarrying.length > 5) {
				inventory = Engine.GetGUIObjectByName("inventory4_5");
				resource = entState.resourceCarrying[5];
				inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
				inventory.tooltip = resource.amount;
				inventory.hidden = false;
			}
			
			if (entState.resourceCarrying.length == 7) {
				inventory = Engine.GetGUIObjectByName("inventory4_6");
				resource = entState.resourceCarrying[5];
				inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
				inventory.tooltip = resource.amount;
				inventory.hidden = false;
			}
		}
		
		if (entState.resourceCarrying.length >= 8) {
			let inventory = Engine.GetGUIObjectByName("inventory18_1");
			let resource = entState.resourceCarrying[1];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory18_2");
			resource = entState.resourceCarrying[2];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			
			inventory = Engine.GetGUIObjectByName("inventory18_3");
			resource = entState.resourceCarrying[3];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory18_4");
			resource = entState.resourceCarrying[4];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory18_5");
			resource = entState.resourceCarrying[5];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
			
			inventory = Engine.GetGUIObjectByName("inventory18_6");
			resource = entState.resourceCarrying[6];
			inventory.sprite = "stretched:session/icons/resources/" + resource.type + ".png";
			inventory.tooltip = resource.amount;
			inventory.hidden = false;
		}
	}
	
	/*if (entState.hitpoints) {
		let unitHealthBar = Engine.GetGUIObjectByName("healthBar");
		let healthSize = unitHealthBar.size;
		healthSize.rright = 100 * Math.max(0, Math.min(1, entState.hitpoints / entState.maxHitpoints));
		unitHealthBar.size = healthSize;
		unitHealthBar.hidden = false;
	} else {
		 Engine.GetGUIObjectByName("healthBar").hidden = true;
	}*/
	
	if (entState.Temple) {
		Engine.GetGUIObjectByName("Religion").hidden = false;
		Engine.GetGUIObjectByName("Religion").tooltip = "Religion: "+entState.Temple.Religion;
		
		//Update religion icon.
		if (entState.Temple.Icon != "") {
			Engine.GetGUIObjectByName("Religion").sprite = "stretched:session/icons/religions/" + entState.Temple.Icon;
		}
		
	} else if (entState.Emotional) {
		Engine.GetGUIObjectByName("Religion").hidden = false;
		Engine.GetGUIObjectByName("Religion").tooltip = "Religion: "+entState.Emotional.Religion;
		
		//Update religion icon.
		if (entState.Emotional.ReligionIcon != "") {
			Engine.GetGUIObjectByName("Religion").sprite = "stretched:session/icons/religions/" + entState.Emotional.ReligionIcon;
		}
		
	} else {
		Engine.GetGUIObjectByName("Religion").hidden = true;
	}
	
	Engine.GetGUIObjectByName("emotion").tooltip = "";
	Engine.GetGUIObjectByName("emotion").hidden = true;
	Engine.GetGUIObjectByName("Star").hidden = true;
	Engine.GetGUIObjectByName("Likes").hidden = true;
	Engine.GetGUIObjectByName("LikesIcon").hidden = true;
	Engine.GetGUIObjectByName("DislikesIcon").hidden = true;
	Engine.GetGUIObjectByName("Dislikes").hidden = true;
	Engine.GetGUIObjectByName("Aspiration").hidden = true;
	Engine.GetGUIObjectByName("AspirationBackground").hidden = true;
	if (entState.Emotional) {
		if (entState.Emotional.Sick) {
			Engine.GetGUIObjectByName("emotion").tooltip = "I am sick, bring me to a temple or druid for treatment.";
		}
		if (entState.Emotional.Icon == "angry.png") {
			Engine.GetGUIObjectByName("emotion").tooltip = "I am unhappy with you and will not follow your orders until you improve my living conditions.";
		}
		
		Engine.GetGUIObjectByName("emotion").hidden = false;
		
		if (entState.Emotional.Star) Engine.GetGUIObjectByName("Star").hidden = false;
		
		Engine.GetGUIObjectByName("AspirationBackground").hidden = false;
		Engine.GetGUIObjectByName("Aspiration").hidden = false;
		Engine.GetGUIObjectByName("Aspiration").sprite = entState.Emotional.Aspiration;
		Engine.GetGUIObjectByName("Aspiration").tooltip = "I want to be a "+entState.Emotional.Aspiration;
		
		if (entState.Emotional.Likes.Name != entState.Emotional.Dislikes.Name) {
			Engine.GetGUIObjectByName("Likes").hidden = false;
			Engine.GetGUIObjectByName("Dislikes").hidden = false;
			
			Engine.GetGUIObjectByName("DislikesIcon").hidden = false
			Engine.GetGUIObjectByName("LikesIcon").hidden = false;
			
			Engine.GetGUIObjectByName("LikesIcon").sprite = "stretched:session/portraits/"+entState.Emotional.Likes.Icon;
			Engine.GetGUIObjectByName("DislikesIcon").sprite = "stretched:session/portraits/"+entState.Emotional.Dislikes.Icon;
			
			Engine.GetGUIObjectByName("LikesIcon").tooltip = "I like "+entState.Emotional.Likes.Name.plural();
			Engine.GetGUIObjectByName("DislikesIcon").tooltip = "I dislike "+entState.Emotional.Dislikes.Name.plural();
		}
	} else {
		Engine.GetGUIObjectByName("Aspiration").sprite = "";
		Engine.GetGUIObjectByName("Likes").tooltip = "";
		Engine.GetGUIObjectByName("Dislikes").tooltip = "";
	}
		
	if (entState.Hunger && entState.Hunger.Hungry) {
		Engine.GetGUIObjectByName("action_bar").hidden = true;
	} else {
		Engine.GetGUIObjectByName("action_bar").hidden = false;
	}
		
	/*if (entState.Hunger) {
		let unitHealthBar = Engine.GetGUIObjectByName("hungerBar");
		let healthSize = unitHealthBar.size;
		healthSize.rright = 100 * Math.max(0, Math.min(1, entState.Hunger.Level / entState.Hunger.Max));
		unitHealthBar.size = healthSize;
		unitHealthBar.hidden = false;
	} else {
		Engine.GetGUIObjectByName("hungerBar").hidden = true;
	}*/

	Engine.GetGUIObjectByName("emotion").sprite = entState.Emotional ? entState.Emotional.Icon : "BackgroundBlack";
	
	/*let genericName = entState.identity.name;
	// If packed, add that to the generic name (reduces template clutter)
	if (genericName && template.pack && template.pack.state == "packed")
		genericName = sprintf(translate("%(genericName)s — Packed"), { "genericName": genericName });
	let playerState = g_Players[entState.player];

	let civName = g_CivData[playerState.civ].Name;
	let civEmblem = g_CivData[playerState.civ].Emblem;

	let playerName = playerState.name;
	let playerColor = rgbToGuiColor(g_DisplayedPlayerColors[entState.player], 128);

	// Indicate disconnected players by prefixing their name
	if (g_Players[entState.player].offline)
		playerName = sprintf(translate("\\[OFFLINE] %(player)s"), { "player": playerName });

	// Rank
	if (entState.identity && entState.identity.rank && entState.identity.classes)
	{
		Engine.GetGUIObjectByName("rankIcon").tooltip = sprintf(translate("%(rank)s Rank"), {
			"rank": translateWithContext("Rank", entState.identity.rank)
		});
		Engine.GetGUIObjectByName("rankIcon").sprite = "stretched:session/icons/ranks/" + entState.identity.rank + ".png";
		Engine.GetGUIObjectByName("rankIcon").hidden = false;
	}
	else
	{
		Engine.GetGUIObjectByName("rankIcon").hidden = true;
		Engine.GetGUIObjectByName("rankIcon").tooltip = "";
	}

	let showHealth = entState.hitpoints;
	let showResource = entState.resourceSupply;

	let healthSection = Engine.GetGUIObjectByName("healthSection");
	let captureSection = Engine.GetGUIObjectByName("captureSection");
	let resourceSection = Engine.GetGUIObjectByName("resourceSection");
	let sectionPosTop = Engine.GetGUIObjectByName("sectionPosTop");
	let sectionPosMiddle = Engine.GetGUIObjectByName("sectionPosMiddle");
	let sectionPosBottom = Engine.GetGUIObjectByName("sectionPosBottom");

	// Hitpoints
	healthSection.hidden = !showHealth;
	if (showHealth)
	{
		let unitHealthBar = Engine.GetGUIObjectByName("healthBar");
		let healthSize = unitHealthBar.size;
		healthSize.rright = 100 * Math.max(0, Math.min(1, entState.hitpoints / entState.maxHitpoints));
		unitHealthBar.size = healthSize;
		Engine.GetGUIObjectByName("healthStats").caption = sprintf(translate("%(hitpoints)s / %(maxHitpoints)s"), {
			"hitpoints": Math.ceil(entState.hitpoints),
			"maxHitpoints": Math.ceil(entState.maxHitpoints)
		});

		healthSection.size = sectionPosTop.size;
		captureSection.size = showResource ? sectionPosMiddle.size : sectionPosBottom.size;
		resourceSection.size = showResource ? sectionPosBottom.size : sectionPosMiddle.size;
	}
	else
	{
		captureSection.size = sectionPosBottom.size;
		resourceSection.size = sectionPosTop.size;
	}

	// CapturePoints
	captureSection.hidden = !entState.capturePoints;
	if (entState.capturePoints)
	{
		let setCaptureBarPart = function(playerID, startSize) {
			let unitCaptureBar = Engine.GetGUIObjectByName("captureBar[" + playerID + "]");
			let sizeObj = unitCaptureBar.size;
			sizeObj.rleft = startSize;

			let size = 100 * Math.max(0, Math.min(1, entState.capturePoints[playerID] / entState.maxCapturePoints));
			sizeObj.rright = startSize + size;
			unitCaptureBar.size = sizeObj;
			unitCaptureBar.sprite = "color:" + rgbToGuiColor(g_DisplayedPlayerColors[playerID], 128);
			unitCaptureBar.hidden = false;
			return startSize + size;
		};

		// first handle the owner's points, to keep those points on the left for clarity
		let size = setCaptureBarPart(entState.player, 0);

		for (let i in entState.capturePoints)
			if (i != entState.player)
				size = setCaptureBarPart(i, size);

		let captureText = sprintf(translate("%(capturePoints)s / %(maxCapturePoints)s"), {
			"capturePoints": Math.ceil(entState.capturePoints[entState.player]),
			"maxCapturePoints": Math.ceil(entState.maxCapturePoints)
		});

		let showSmallCapture = showResource && showHealth;
		Engine.GetGUIObjectByName("captureStats").caption = showSmallCapture ? "" : captureText;
		Engine.GetGUIObjectByName("capture").tooltip = showSmallCapture ? captureText : "";
	}

	// Experience
	Engine.GetGUIObjectByName("experience").hidden = !entState.promotion;
	if (entState.promotion)
	{
		let experienceBar = Engine.GetGUIObjectByName("experienceBar");
		let experienceSize = experienceBar.size;
		experienceSize.rtop = 100 - (100 * Math.max(0, Math.min(1, 1.0 * +entState.promotion.curr / +entState.promotion.req)));
		experienceBar.size = experienceSize;

		if (entState.promotion.curr < entState.promotion.req)
			Engine.GetGUIObjectByName("experience").tooltip = sprintf(translate("%(experience)s %(current)s / %(required)s"), {
				"experience": "[font=\"sans-bold-13\"]" + translate("Experience:") + "[/font]",
				"current": Math.floor(entState.promotion.curr),
				"required": entState.promotion.req
			});
		else
			Engine.GetGUIObjectByName("experience").tooltip = sprintf(translate("%(experience)s %(current)s"), {
				"experience": "[font=\"sans-bold-13\"]" + translate("Experience:") + "[/font]",
				"current": Math.floor(entState.promotion.curr)
			});
	}

	// Resource stats
	resourceSection.hidden = !showResource;
	if (entState.resourceSupply)
	{
		let resources = entState.resourceSupply.isInfinite ? translate("∞") :  // Infinity symbol
			sprintf(translate("%(amount)s / %(max)s"), {
				"amount": Math.ceil(+entState.resourceSupply.amount),
				"max": entState.resourceSupply.max
			});

		let unitResourceBar = Engine.GetGUIObjectByName("resourceBar");
		let resourceSize = unitResourceBar.size;

		resourceSize.rright = entState.resourceSupply.isInfinite ? 100 :
			100 * Math.max(0, Math.min(1, +entState.resourceSupply.amount / +entState.resourceSupply.max));
		unitResourceBar.size = resourceSize;

		Engine.GetGUIObjectByName("resourceLabel").caption = sprintf(translate("%(resource)s:"), {
			"resource": getResourceTypeDisplayName(entState.resourceSupply.type)
		});
		Engine.GetGUIObjectByName("resourceStats").caption = resources;

	}

	let resourceCarryingIcon = Engine.GetGUIObjectByName("resourceCarryingIcon");
	let resourceCarryingText = Engine.GetGUIObjectByName("resourceCarryingText");
	resourceCarryingIcon.hidden = false;
	resourceCarryingText.hidden = false;

	// Resource carrying
	if (entState.resourceCarrying && entState.resourceCarrying.length)
	{
		// We should only be carrying one resource type at once, so just display the first
		let carried = entState.resourceCarrying[0];
		resourceCarryingIcon.sprite = "stretched:session/icons/resources/" + carried.type + ".png";
		resourceCarryingText.caption = sprintf(translate("%(amount)s / %(max)s"), { "amount": carried.amount, "max": carried.max });
		resourceCarryingIcon.tooltip = "";
	}
	// Use the same indicators for traders
	else if (entState.trader && entState.trader.goods.amount)
	{
		resourceCarryingIcon.sprite = "stretched:session/icons/resources/" + entState.trader.goods.type + ".png";
		let totalGain = entState.trader.goods.amount.traderGain;
		if (entState.trader.goods.amount.market1Gain)
			totalGain += entState.trader.goods.amount.market1Gain;
		if (entState.trader.goods.amount.market2Gain)
			totalGain += entState.trader.goods.amount.market2Gain;
		resourceCarryingText.caption = totalGain;
		resourceCarryingIcon.tooltip = sprintf(translate("Gain: %(gain)s"), {
			"gain": getTradingTooltip(entState.trader.goods.amount)
		});
	}
	// And for number of workers
	else if (entState.foundation)
	{
		resourceCarryingIcon.sprite = "stretched:session/icons/repair.png";
		resourceCarryingIcon.tooltip = getBuildTimeTooltip(entState);
		resourceCarryingText.caption = entState.foundation.numBuilders ?
			Engine.FormatMillisecondsIntoDateStringGMT(entState.foundation.buildTime.timeRemaining * 1000, translateWithContext("countdown format", "m:ss")) : "";
	}
	else if (entState.resourceSupply && (!entState.resourceSupply.killBeforeGather || !entState.hitpoints))
	{
		resourceCarryingIcon.sprite = "stretched:session/icons/repair.png";
		resourceCarryingText.caption = sprintf(translate("%(amount)s / %(max)s"), {
			"amount": entState.resourceSupply.numGatherers,
			"max": entState.resourceSupply.maxGatherers
		});
		Engine.GetGUIObjectByName("resourceCarryingIcon").tooltip = translate("Current/max gatherers");
	}
	else if (entState.repairable && entState.needsRepair)
	{
		resourceCarryingIcon.sprite = "stretched:session/icons/repair.png";
		resourceCarryingIcon.tooltip = getRepairTimeTooltip(entState);
		resourceCarryingText.caption = entState.repairable.numBuilders ?
			Engine.FormatMillisecondsIntoDateStringGMT(entState.repairable.buildTime.timeRemaining * 1000, translateWithContext("countdown format", "m:ss")) : "";
	}
	else
	{
		resourceCarryingIcon.hidden = true;
		resourceCarryingText.hidden = true;
	}

	
	Engine.GetGUIObjectByName("player").caption = playerName;
	Engine.GetGUIObjectByName("playerColorBackground").sprite = "color:" + playerColor;
	Engine.GetGUIObjectByName("generic").caption = genericName == specificName ? "" :
		sprintf(translate("(%(genericName)s)"), {
			"genericName": genericName
		});

	let isGaia = playerState.civ == "gaia";
	Engine.GetGUIObjectByName("playerCivIcon").sprite = isGaia ? "" : "stretched:grayscale:" + civEmblem;
	Engine.GetGUIObjectByName("player").tooltip = isGaia ? "" : civName;

	// TODO: we should require all entities to have icons
	*/
	
	Engine.GetGUIObjectByName("specific").caption = specificName;
	
	/*Engine.GetGUIObjectByName("icon").sprite = template.icon ? ("stretched:session/portraits/" + template.icon) : "BackgroundBlack";*/
	
	/*
	
	if (template.icon)
		Engine.GetGUIObjectByName("iconBorder").onPressRight = () => {
			showTemplateDetails(entState.template);
		};

	Engine.GetGUIObjectByName("attackAndArmorStats").tooltip = [
		getAttackTooltip,
		getSplashDamageTooltip,
		getHealerTooltip,
		getArmorTooltip,
		getGatherTooltip,
		getSpeedTooltip,
		getGarrisonTooltip,
		getProjectilesTooltip,
		getResourceTrickleTooltip,
		getLootTooltip
	].map(func => func(entState)).filter(tip => tip).join("\n");

	let iconTooltips = [];

	if (genericName)
		iconTooltips.push("[font=\"sans-bold-16\"]" + genericName + "[/font]");

	iconTooltips = iconTooltips.concat([
		getVisibleEntityClassesFormatted,
		getAurasTooltip,
		getEntityTooltip,
		showTemplateViewerOnRightClickTooltip
	].map(func => func(template)));

	Engine.GetGUIObjectByName("iconBorder").tooltip = iconTooltips.filter(tip => tip).join("\n");

	Engine.GetGUIObjectByName("detailsAreaSingle").hidden = false;
	Engine.GetGUIObjectByName("detailsAreaMultiple").hidden = true;
*/}

// Fills out information for multiple entities
function displayMultiple(entStates)
{/*
	let averageHealth = 0;
	let maxHealth = 0;
	let maxCapturePoints = 0;
	let capturePoints = (new Array(g_MaxPlayers + 1)).fill(0);
	let playerID = 0;
	let totalCarrying = {};
	let totalLoot = {};

	for (let entState of entStates)
	{
		playerID = entState.player; // trust that all selected entities have the same owner
		if (entState.hitpoints)
		{
			averageHealth += entState.hitpoints;
			maxHealth += entState.maxHitpoints;
		}
		if (entState.capturePoints)
		{
			maxCapturePoints += entState.maxCapturePoints;
			capturePoints = entState.capturePoints.map((v, i) => v + capturePoints[i]);
		}

		let carrying = calculateCarriedResources(
			entState.resourceCarrying || null,
			entState.trader && entState.trader.goods
		);

		if (entState.loot)
			for (let type in entState.loot)
				totalLoot[type] = (totalLoot[type] || 0) + entState.loot[type];

		for (let type in carrying)
		{
			totalCarrying[type] = (totalCarrying[type] || 0) + carrying[type];
			totalLoot[type] = (totalLoot[type] || 0) + carrying[type];
		}
	}

	Engine.GetGUIObjectByName("healthMultiple").hidden = averageHealth <= 0;
	if (averageHealth > 0)
	{
		let unitHealthBar = Engine.GetGUIObjectByName("healthBarMultiple");
		let healthSize = unitHealthBar.size;
		healthSize.rtop = 100 - 100 * Math.max(0, Math.min(1, averageHealth / maxHealth));
		unitHealthBar.size = healthSize;

		Engine.GetGUIObjectByName("healthMultiple").tooltip = getCurrentHealthTooltip({
			"hitpoints": averageHealth,
			"maxHitpoints": maxHealth
		});
	}

	Engine.GetGUIObjectByName("captureMultiple").hidden = maxCapturePoints <= 0;
	if (maxCapturePoints > 0)
	{
		let setCaptureBarPart = function(pID, startSize)
		{
			let unitCaptureBar = Engine.GetGUIObjectByName("captureBarMultiple[" + pID + "]");
			let sizeObj = unitCaptureBar.size;
			sizeObj.rtop = startSize;

			let size = 100 * Math.max(0, Math.min(1, capturePoints[pID] / maxCapturePoints));
			sizeObj.rbottom = startSize + size;
			unitCaptureBar.size = sizeObj;
			unitCaptureBar.sprite = "color:" + rgbToGuiColor(g_DisplayedPlayerColors[pID], 128);
			unitCaptureBar.hidden = false;
			return startSize + size;
		};

		let size = 0;
		for (let i in capturePoints)
			if (i != playerID)
				size = setCaptureBarPart(i, size);

		// last handle the owner's points, to keep those points on the bottom for clarity
		setCaptureBarPart(playerID, size);

		Engine.GetGUIObjectByName("captureMultiple").tooltip = getCurrentHealthTooltip(
			{
				"hitpoints": capturePoints[playerID],
				"maxHitpoints": maxCapturePoints
			},
			translate("Capture Points:"));
	}

	let numberOfUnits = Engine.GetGUIObjectByName("numberOfUnits");
	numberOfUnits.caption = entStates.length;
	numberOfUnits.tooltip = "";

	if (Object.keys(totalCarrying).length)
		numberOfUnits.tooltip = sprintf(translate("%(label)s %(details)s\n"), {
			"label": headerFont(translate("Carrying:")),
			"details": bodyFont(Object.keys(totalCarrying).filter(
				res => totalCarrying[res] != 0).map(
				res => sprintf(translate("%(type)s %(amount)s"),
					{ "type": resourceIcon(res), "amount": totalCarrying[res] })).join("  "))
		});

	if (Object.keys(totalLoot).length)
		numberOfUnits.tooltip += sprintf(translate("%(label)s %(details)s"), {
			"label": headerFont(translate("Loot:")),
			"details": bodyFont(Object.keys(totalLoot).filter(
				res => totalLoot[res] != 0).map(
				res => sprintf(translate("%(type)s %(amount)s"),
					{ "type": resourceIcon(res), "amount": totalLoot[res] })).join("  "))
		});

	// Unhide Details Area
	Engine.GetGUIObjectByName("detailsAreaMultiple").hidden = false;
	Engine.GetGUIObjectByName("detailsAreaSingle").hidden = true;
*/}

// Updates middle entity Selection Details Panel and left Unit Commands Panel
function updateSelectionDetails()
{
	//let supplementalDetailsPanel = Engine.GetGUIObjectByName("supplementalSelectionDetails");
	let detailsPanel = Engine.GetGUIObjectByName("selectionDetails");
	//let commandsPanel = Engine.GetGUIObjectByName("unitCommands");

	let entStates = [];

	for (let sel of g_Selection.toList())
	{
		let entState = GetEntityState(sel);
		if (!entState)
			continue;
		entStates.push(entState);
	}

	if (entStates.length == 0)
	{
		//Engine.GetGUIObjectByName("detailsAreaMultiple").hidden = true;
		//Engine.GetGUIObjectByName("detailsAreaSingle").hidden = true;
		//hideUnitCommands();

		//supplementalDetailsPanel.hidden = true;
		detailsPanel.hidden = true;
		//commandsPanel.hidden = true;
		return;
	}
	
	// Show basic details.
	detailsPanel.hidden = false;

	// Fill out commands panel for specific unit selected (or first unit of primary group)
	updateUnitCommands(entStates, {hidden: false},  Engine.GetGUIObjectByName("unitModPanel"));

	// Fill out general info and display it
	if (entStates.length == 1)
		displaySingle(entStates[0]);
	else
		displayMultiple(entStates);

	// Show health bar for garrisoned units if the garrison panel is visible
	//if (Engine.GetGUIObjectByName("unitGarrisonPanel") && !Engine.GetGUIObjectByName("unitGarrisonPanel").hidden)
		//updateGarrisonHealthBar(entStates[0], g_Selection.toList());
}

function tradingGainString(gain, owner)
{
	// Translation: Used in the trading gain tooltip
	return sprintf(translate("%(gain)s (%(player)s)"), {
		"gain": gain,
		"player": GetSimState().players[owner].name
	});
}

/**
 * Returns a message with the details of the trade gain.
 */
function getTradingTooltip(gain)
{
	if (!gain)
		return "";

	let markets = [
		{ "gain": gain.market1Gain, "owner": gain.market1Owner },
		{ "gain": gain.market2Gain, "owner": gain.market2Owner }
	];

	let primaryGain = gain.traderGain;

	for (let market of markets)
		if (market.gain && market.owner == gain.traderOwner)
			// Translation: Used in the trading gain tooltip to concatenate profits of different players
			primaryGain += translate("+") + market.gain;

	let tooltip = tradingGainString(primaryGain, gain.traderOwner);

	for (let market of markets)
		if (market.gain && market.owner != gain.traderOwner)
			tooltip +=
				translateWithContext("Separation mark in an enumeration", ", ") +
				tradingGainString(market.gain, market.owner);

	return tooltip;
} 
