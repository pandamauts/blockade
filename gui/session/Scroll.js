var MenuOpen = false;

function CreateMenu() {
	var elements = ["Help", "Options", "Save", "Exit"];
	
	const width = 250;
	var height = 64*elements.length;
	
	
	let menu = gui.Get("Menu");
	{
		menu.SetSize(width, height);

		menu.AttachRight();
		menu.SetOffset(-width, -height+50);
		
		menu.SetBackgroundImage("Scroll")
		menu.Update();
	}
	
	{
		let Title = gui.Get("Realm of Order")
		
		Title.object.onPress = function() {
			if (MenuOpen) {
				menu.SetOffset(-width, -height+50);
				menu.Update();
				MenuOpen = false;
			} else {
				menu.SetOffset(-width, 0);
				menu.Update();
				MenuOpen = true;
			}
		}
		Title.SetText("Realm of Order");
		Title.SetOffset(0, height-75);
		Title.SetSize(width, 100);
		Title.Update();
	}
	
	gui.Get("Help").object.onPress = openManual;
	gui.Get("Options").object.onPress = optionsMenuButton;
	gui.Get("Save").object.onPress = openSave;
	gui.Get("Exit").object.onPress = exitMenuButton;
	
	var i = 0;
	for (let element of elements) {
		let object = gui.Get(element);
		
		
		object.SetText(element);
		object.SetOffset(0, 50*i)
		object.SetSize(width, 50);
		object.Update();
		i++;
	}
}
