

g_UnitActions["gather"]["getActionInfo"] = function(entState, targetState)
{
	if (!targetState.resourceSupply)
		return false;

	let resource = findGatherType(entState, targetState.resourceSupply);
	if (!resource)
		return false;

	return {
		"possible": true
		//"cursor": "action-gather-" + resource
	};
}

g_UnitActions["returnresource"]["getActionInfo"] = function(entState, targetState)
{
	if (!targetState.resourceDropsite)
		return false;

	let playerState = GetSimState().players[entState.player];
	if (playerState.hasSharedDropsites && targetState.resourceDropsite.shared)
	{
		if (!playerCheck(entState, targetState, ["Player", "MutualAlly"]))
			return false;
	}
	else if (!playerCheck(entState, targetState, ["Player"]))
		return false;

	if (!entState.resourceCarrying || !entState.resourceCarrying.length)
		return false;

	let carriedType = entState.resourceCarrying[0].type;
	
	if (targetState.resourceDropsite.types.indexOf(carriedType) == -1)
		return false;

	return {
		"possible": true
		//"cursor": "action-return-" + carriedType
	};
}

g_UnitActions["heal"]["getActionInfo"] = function(entState, targetState) {
	if (!entState.heal ||
		!hasClass(targetState, "Unit") || !targetState.needsHeal ||
		entState.id == targetState.id) // Healers can't heal themselves.
		return false;

	let unhealableClasses = entState.heal.unhealableClasses;
	if (MatchesClassList(targetState.identity.classes, unhealableClasses))
		return false;

	let healableClasses = entState.heal.healableClasses;
	if (!MatchesClassList(targetState.identity.classes, healableClasses))
		return false;

	return { "possible": true };
}

g_UnitActions["setup-trade-route"]["getActionInfo"] = function(entState, targetState)
{
	if (targetState.foundation || !entState.trader || !targetState.market ||
		playerCheck(entState, targetState, ["Enemy"]) ||
		!(targetState.market.land && hasClass(entState, "Organic") ||
			targetState.market.naval && hasClass(entState, "Ship")))
		return false;

	let tradingDetails = Engine.GuiInterfaceCall("GetTradingDetails", {
		"trader": entState.id,
		"target": targetState.id
	});

	if (!tradingDetails)
		return false;

	let tooltip;
	switch (tradingDetails.type)
	{
	case "is first":
		tooltip = translate("Origin trade market.") + "\n";
		if (tradingDetails.hasBothMarkets)
			tooltip += sprintf(translate("Gain: %(gain)s"), {
				"gain": getTradingTooltip(tradingDetails.gain)
			});
		else
			tooltip += translate("Right-click on another market to set it as a destination trade market.");
		break;

	case "is second":
		tooltip = translate("Destination trade market.") + "\n" +
			sprintf(translate("Gain: %(gain)s"), {
				"gain": getTradingTooltip(tradingDetails.gain)
			});
		break;

	case "set first":
		tooltip = translate("Right-click to set as origin trade market");
		break;

	case "set second":

		tooltip = translate("Right-click to set as destination trade market.") + "\n" +
			sprintf(translate("Gain: %(gain)s"), {
				"gain": getTradingTooltip(tradingDetails.gain)
			});

		break;
	}

	return {
		"possible": true,
		"tooltip": tooltip
	};
}

g_UnitActions["garrison"]["hotkeyActionCheck"] = function(target, selection) {
	let actionInfo = getActionInfo("garrison", target, selection);

	if (!actionInfo.possible)
		return false;

	return {
		"type": "garrison",
		"cursor": "action-garrison",
		"tooltip": actionInfo.tooltip,
		"target": target
	};
}

g_UnitActions["guard"]["hotkeyActionCheck"] = function(target, selection) {
	if (!getActionInfo("guard", target, selection).possible)
		return false;

	return {
		"type": "guard",
		"cursor": "action-guard",
		"target": target
	};
}

g_UnitActions["remove-guard"]["hotkeyActionCheck"] = function(target, selection) {
	if (!getActionInfo("remove-guard", target, selection).possible ||
		!someGuarding(selection))
		return false;

	return {
		"type": "remove-guard",
		"cursor": "action-remove-guard"
	};
}

g_UnitActions["guard"]["getActionInfo"] = function(entState, targetState) {
	if (!targetState.unitAI || targetState.resourceSupply || !playerCheck(entState, targetState, ["Player", "Ally"]) ||
		!entState.unitAI || !entState.unitAI.canGuard)
		return false;

	return { "possible": true };
},

g_UnitActions["remove-guard"]["getActionInfo"] = function(entState, targetState) {
	if (entState.unitAI && entState.unitAI.isGuarding)
		return { "possible": true };

	return false;
}

g_UnitActions["patrol"]["hotkeyActionCheck"] = function(target, selection) {
	if (!someCanPatrol(selection) ||
		!Engine.HotkeyIsPressed("session.garrison") ||
		!getActionInfo("patrol", target, selection).possible)
		return false;
	return {
		"type": "patrol",
		"cursor": "action-patrol",
		"target": target
	};
}

g_UnitActions["patrol"]["specificness"] = 20;
