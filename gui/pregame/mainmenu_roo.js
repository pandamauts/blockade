
let old_updateMenuPosition = updateMenuPosition;
let roo_first = false;

updateMenuPosition = function() {
	old_updateMenuPosition();
	
	if (!roo_first) {
		let Arguments = Engine.ReadJSONFile("Arguments.json");
		if (!Arguments) return;
		
		if (Arguments.LoadLastSavedGame) {
			let games = Engine.GetSavedGames();
			let metadata = Engine.StartSavedGame(games[games.length-1].id);
			
			let pData = metadata.initAttributes.settings.PlayerData[metadata.playerID];
			
			if (metadata)
				Engine.SwitchGuiPage("page_loading.xml", {
					"attribs": metadata.initAttributes,
					"playerAssignments": {
						"local": {
							"name": pData.Name ? pData.Name : singleplayerName(),
							"player": metadata.playerID
						}
					},
					"savedGUIData": metadata.gui
				});
		}
		roo_first = true;
	}
}
