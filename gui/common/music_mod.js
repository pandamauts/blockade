Music.prototype.resetTracks = function()
{
	this.tracks = {
		"MENU": ["Honor_Bound.ogg"].concat(shuffleArray([
			"An_old_Warhorse_goes_to_Pasture.ogg",
			"Calm_Before_the_Storm.ogg",
			"Juno_Protect_You.ogg"
		])),
		"PEACE": [
			"Tale_of_Warriors.ogg",
			"Tavern_in_the_Mist.ogg",
			"The_Road_Ahead.ogg",
			
			"Connecting_Rainbows.ogg",
			"Magic_in_the_Garden.ogg",
			"Champ_de_tournesol.ogg",
			"La_Citadelle.ogg",
			"Patience_Party.ogg",
			"The_Britons.ogg",
			"Ambient_Bongos.ogg",
			"The_Story.ogg",
			"Nomadic_Sunset.ogg",
			"Marked.ogg"
		],
		"BATTLE": [
			"Taiko_1.ogg", 
			"Taiko_2.ogg", 
			"Action_Strike.ogg", 
			"Epic_Boss_Battle.ogg",
			"Night_Attack.ogg",
			"Kings_Trailer.ogg"
		],
		"VICTORY": ["You_are_Victorious!.ogg"],
		"DEFEAT": ["Dried_Tears.ogg"],
		"CUSTOM": []
	};
};
 
